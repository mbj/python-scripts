% Generate noise with spatial correlation in poloidal, radial and toroidal
% directions
% Sampling on spherical grid with spatial correlation
close all
clear all
clc

Rmax = 500;
rmin = 100;
rstart = 10;

L = 2*pi*Rmax;
% Parameters of the noise distribution
mu    = 0;     % mean of the noise (spatially constant)
sigma = 1;     % standard deviation for the noise (spatially constant)
Lr     = rmin/10;  % correlation length in radial direction
Lp     = 2*pi*rmin/20; % Correlation "length" in poloidal direction
Lt    = 2*pi*Rmax; % Correlation "length" in toroidal direction
%Lp = Lr;
Lcorr = Lr/2;

nr = 20; % number of grid points in x-direction
np = 50; % number of grid points in y-direction
nt = 10;
tol = 10^-15;
r = (rstart/rmin:0.9/(nr-1):1)*rmin;
p = (0:1/(np-1):1)*2*pi;
t = (0:1/(nt-1):1)*2*pi;
[P,R,T] = meshgrid(p,r,t);
% Vector of mean at each grid point
Mu = mu*ones(nr*np*nt,1);

is = (1:np*nr*nt);
js = (1:np*nr*nt);
vs = sigma^2*ones(np*nr*nt,1);

% Construct covariance matrices
Sigma_corr    = sparse(is,js,vs,nr*np*nt,nr*np*nt);
%Sigma_corr = zeros(nr*np);
Sigma_no_corr = sparse(is,js,vs,nr*np*nt,nr*np*nt);

for h = 1:nt
    for j = 1:np
        for i = 1:nr

            k = i + (j-1)*nr + (h-1)*nr*np;

            % Main diagonal: variance
          %  Sigma_no_corr(k,k) = sigma^2;
           % Sigma_corr(k,k)    = sigma^2;

            % Off-diagonal: spatial correlation
            nnb = 4; 
       %     for n = j-nnb:j+nnb
        %        for m = i-nnb:i+nnb
         %           if (m ==i || n==j) && not(m==i && n==j) %&& not(m==0 || m==nr+1 ||n==0 ||n == np+1)
          %              mc = m+nr*(m<=0) - nr*(m>=nr+1);
           %             nc = n+np*(n<=0) - np*(n>=np+1);
            %            l = mc + (nc-1)*nr;
             %           Sigma_corr(k,l) = sigma^2*exp(-sqrt((R(mc,nc)-R(i,j))^2)/Lr)*exp(-(abs(P(mc,nc)-P(i,j))*(R(mc,nc)+R(i,j))/2)/Lp);
              %      end
               % end
            %end
         % Alternatief
         for p = 1:nt
            for n=1:np
                for m=1:nr
                    l = m+(n-1)*nr+(p-1)*nr*np;
                    if l ~= k
                        distance = ((R(m,n,p)-R(i,j,h))^2+ (abs(P(m,n,p)-P(i,j,h))*(R(m,n,p)+R(i,j,h))/2))^0.5;
                        if distance < Lcorr
                            Sigma_corr(k,l) = sigma^2*exp(-sqrt((R(m,n,p)-R(i,j,h))^2)/Lr)...
                                *exp(-(abs(P(m,n,p)-P(i,j,h))*(R(m,n,p)+R(i,j,h))/2)/Lp)...
                                *exp(-(abs(T(m,n,p)-T(i,j,h))*(Rmax+R(m,n,p)*cos(P(m,n,p))+Rmax+R(i,j,h)*cos(P(i,j,h)))/2)/Lt);
                             b=1;  % test bp
                            %Sigma_corr(k,l) = sigma^2*exp(-distance/Lcorr);
                        end
                    end
                end
            end
         end

        end
    end
end

    b = issymmetric(Sigma_corr)
    %d = eig(Sigma_corr)

    [U,S,V] = svds(Sigma_corr,nr*np*nt);
    z = 0+randn(nr*np*nt,1)*sigma;
    noise_corr = U *sqrt(S)*z;
    % Generate the noise from multivariate normal distributions
    noise_no_corr = mvnrnd(Mu,Sigma_no_corr);
    %noise_corr    = mvnrnd(Mu,Sigma_corr);

   fileID = fopen('CorrelatedNoise','w');
   fprintf(fileID,'%6s \n',noise_corr);
   fclose(fileID);

    % Reshape onto mesh
   noise_no_corr = reshape(noise_no_corr,nr,np,nt);
   noise_corr = reshape(noise_corr,nr,np,nt);

   

    % Plot
    %figure;surf(R,P,noise_no_corr);title('Independent');
    %figure;surf(R,P,noise_corr); title('Spatial correlation');

  