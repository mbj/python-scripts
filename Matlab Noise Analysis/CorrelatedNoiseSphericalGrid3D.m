% Generate noise with spatial correlation in poloidal, radial and toroidal
% directions
% Sampling on spherical grid with spatial correlation
% 3D case
close all
clear all
clc

Rmax = 500;
rmin = 100;
rstart = 10;

L = 2*pi*Rmax;
% Parameters of the noise distribution
mu    = 0;     % mean of the noise (spatially constant)
sigma = 1;     % standard deviation for the noise (spatially constant)
Lr     = rmin/10;  % correlation length in radial direction
Lp     = 2*pi*rmin/10; % Correlation "length" in poloidal direction
Lt    = 2*pi*Rmax/10; % Correlation "length" in toroidal direction


nr = 5; 
np = 10; 
nt = 2;

r = (rstart/rmin:0.9/(nr-1):1)*rmin;
p = (0:1/(np-1):1)*2*pi;
t = (0:1/(nt-1):1)*2*pi;
[P,R,T] = meshgrid(p,r,t);

% Vector of mean at each grid point
Mu = mu*ones(nr*np*nt,1);

% Construct covariance matrices
Sigma_corr    = zeros(nr*np*nt);
Sigma_no_corr = zeros(nr*np*nt);

for h = 1:nt
    for j = 1:np
        for i = 1:nr
        
            k = i + (j-1)*nr + (h-1)*nr*np;
        
            % Main diagonal: variance
            Sigma_no_corr(k,k) = sigma^2;
            Sigma_corr(k,k)    = sigma^2;
        
         % Off-diagonal: spatial correlation
            for p = 1:nt
                for n = 1:np
                    for m = 1:nr
                        l = m + (n-1)*nr + (p-1)*nr*np;
                        if l ~= k
                            Sigma_corr(k,l) = sigma^2*exp(-sqrt((R(m,n,p)-R(i,j,h))^2)/Lr)...
                                *exp(-(abs(P(m,n,p)-P(i,j,h))*(R(m,n,p)+R(i,j,h))/2)/Lp) ...
                                *exp(-(abs(T(m,n,p)-T(i,j,h))*(Rmax+R(m,n,p)*cos(P(m,n,p))+Rmax+R(i,j,h)*cos(P(i,j,h)))/2)/Lt);
                        end    
                    end
                end
            end
        end
    end
end

b = issymmetric(Sigma_corr)
d = eig(Sigma_corr)
% Generate the noise from multivariate normal distributions
noise_no_corr = mvnrnd(Mu,Sigma_no_corr);
noise_corr    = mvnrnd(Mu,Sigma_corr);

% To extract the noise, store in same format as BFSTREN (1 noise value for
% every grid point) => then shift to cell centers


% Reshape onto mesh
noise_no_corr = reshape(noise_no_corr,nr,np,nt);
noise_corr = reshape(noise_corr,nr,np,nt);

% Plot
%figure;surf(R,P,T);title('Independent');
%figure;surf(R,P,T); title('Spatial correlation');