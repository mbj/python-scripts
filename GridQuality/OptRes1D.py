import grid_analyze_version3 as grid_analyze
import sys
import numpy as np
import matplotlib.pyplot as plt
from MeshFunctions import *
import math

nt = 6
nts = [30,60,120,180,360]
nrs = [5,10,20,40,80]
nRad = 20
nPol = 200
nps = [50,100,200,400,800]
Rmajor = 5
Rminor = 1
trange = 360
Rangles = []
Pangles = []
Rskew = []
Pskew = []
Runeven = []
Puneven = [] 
lr = []
lp = []      
print('started')
for l in range(5):
        lr.append(nrs[l])
        nr = nrs[l]
        npol = nPol
        [tor,rad,pol] = create_toroidal_vertices(nt,nr,npol,trange,Rmajor,Rminor)
        tor = [t *math.pi/180 for t in tor]
        result = [0.0,0.0,0.0]
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nt,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,npol,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nr,axis = 0)
        
        for i in range(nr):
            for j in range(npol):
                for k in range(nt):
                    result[i,j,k,0] = rad[k][j*nr+i]
                    result[i,j,k,1] = pol[k][j*nr+i]
                    result[i,j,k,2] = tor[k]
        
        angles_r, angles_tht = grid_analyze.non_orthogonality(result)
        uneven_r, uneven_tht = grid_analyze.unevenness(result)
        skewness_r, skewness_tht = grid_analyze.skewness(result)
        Rangles.append(np.mean(angles_r))
        Pangles.append(np.mean(angles_tht))
        Rskew.append(np.mean(skewness_r))
        Pskew.append(np.mean(skewness_tht))
        Runeven.append(np.mean(uneven_r))
        Puneven.append(np.mean(uneven_tht))
print(Rangles)
print(lr)
print(max(Rangles))
print([r/max(Rangles) for r in Rangles])	
plot1 = plt.figure(1)
plt.xscale('log')
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Grid Quality Parameters (Normalized)')
title = 'Grid Quality (Test Geometry)'

plt.title(title)
plt.plot(lr,[r/max(Rangles) for r in Rangles], label='Unorthogonality Radial')
plt.plot(lr,[r/max(Pangles) for r in Pangles], label='Unorthogonality Poloidal')
plt.plot(lr,[r/max(Rskew) for r in Rskew], label='Skewness Radial')
plt.plot(lr,[r/max(Pskew) for r in Pskew], label='Skewness Poloidal')
plt.plot(lr,[r/max(Runeven) for r in Runeven], label='Unevenness Radial')
plt.plot(lr,[r/max(Puneven) for r in Puneven], label='Unevenness Poloidal')
plt.legend()
plt.savefig(title+'Radial'+'+pdf')

plot3 = plt.figure(3)
plt.xscale('log')
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Grid Quality Parameters (Normalized)')
title = 'Grid Quality (Test Geometry)'
plt.title(title)
plt.plot(lr,[r for r in Rangles], label='Unorthogonality Radial')
plt.plot(lr,[r for r in Pangles], label='Unorthogonality Poloidal')
plt.plot(lr,[r for r in Rskew], label='Skewness Radial')
plt.plot(lr,[r for r in Pskew], label='Skewness Poloidal')
plt.plot(lr,[r for r in Runeven], label='Unevenness Radial')
plt.plot(lr,[r for r in Puneven], label='Unevenness Poloidal')
plt.legend()
plt.savefig(title+'Radialabs'+'+pdf')

print('rangles',Rangles)
print('pangles',Pangles)
print('rskew',Rskew)
print('pskew',Pskew)
print('run', Runeven)
print('pun',Puneven)
Rangles = []
Pangles = []
Rskew = []
Pskew = []
Runeven = []
Puneven = [] 
lr = []
lp = [] 
for l in range(5):
        lp.append(nps[l])
        nr = nRad
        npol = nps[l]
        [tor,rad,pol] = create_toroidal_vertices(nt,nr,npol,trange,Rmajor,Rminor)
        tor = [t *math.pi/180 for t in tor]
        result = [0.0,0.0,0.0]
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nt,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,npol,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nr,axis = 0)
        
        for i in range(nr):
            for j in range(npol):
                for k in range(nt):
                    result[i,j,k,0] = rad[k][j*nr+i]
                    result[i,j,k,1] = pol[k][j*nr+i]
                    result[i,j,k,2] = tor[k]
        
        angles_r, angles_tht = grid_analyze.non_orthogonality(result)
        uneven_r, uneven_tht = grid_analyze.unevenness(result)
        skewness_r, skewness_tht = grid_analyze.skewness(result)
        Rangles.append(np.mean(angles_r))
        Pangles.append(np.mean(angles_tht))
        Rskew.append(np.mean(skewness_r))
        Pskew.append(np.mean(skewness_tht))
        Runeven.append(np.mean(uneven_r))
        Puneven.append(np.mean(uneven_tht))
	
plot1 = plt.figure(2)
plt.xlabel('Nb Poloidal surfaces')
plt.ylabel('Grid Quality Parameters (Normalized)')
title = 'Grid Quality (Test Geometry)'
plt.xscale('log')

plt.title(title)
plt.plot(lp,[r/max(Rangles) for r in Rangles], label='Unorthogonality Radial')
plt.plot(lp,[r/max(Pangles) for r in Pangles], label='Unorthogonality Poloidal')
plt.plot(lp,[r/max(Rskew) for r in Rskew], label='Skewness Radial')
plt.plot(lp,[r/max(Pskew) for r in Pskew], label='Skewness Poloidal')
plt.plot(lp,[r/max(Runeven) for r in Runeven], label='Unevenness Radial')
plt.plot(lp,[r/max(Puneven) for r in Puneven], label='Unevenness Poloidal')
plt.legend()
plt.savefig(title+'Poloidal'+'.pdf')

plot3 = plt.figure(4)
plt.xlabel('Nb Poloidal surfaces')
plt.ylabel('Grid Quality Parameters (Normalized)')
title = 'Grid Quality (Test Geometry)'
plt.xscale('log')
plt.title(title)
plt.plot(lp,[r for r in Rangles], label='Unorthogonality Radial')
plt.plot(lp,[r for r in Pangles], label='Unorthogonality Poloidal')
plt.plot(lp,[r for r in Rskew], label='Skewness Radial')
plt.plot(lp,[r for r in Pskew], label='Skewness Poloidal')
plt.plot(lp,[r for r in Runeven], label='Unevenness Radial')
plt.plot(lp,[r for r in Puneven], label='Unevenness Poloidal')
plt.legend()
plt.savefig(title+'Poloidalabs'+'+pdf')

print('rangles',Rangles)
print('pangles',Pangles)
print('rskew',Rskew)
print('pskew',Pskew)
print('run', Runeven)
print('pun',Puneven)


plt.show()
