import grid_analyze
import sys
import numpy as np
import matplotlib.pyplot as plt
from MeshFunctions import *
import math

nt = 10
nrs = [5,8,10,12,15,20,25,30,35,40]
nps = [15,25,35,45,55,66,75,85,95,105]
Rmajor = 5
Rminor = 1
trange = 360
Rangles = []
Pangles = []
Rskew = []
Pskew = []
Runeven = []
Puneven = [] 
lr = []
lp = []      
print('started')
for l in range(10):
    for m in range(10):
        print(l)
        print(m)
        lr.append(nrs[l])
        lp.append(nps[m])
        nr = nrs[l]
        npol = nps[m]
        [tor,rad,pol] = create_toroidal_vertices(nt,nr,npol,trange,Rmajor,Rminor)
        tor = [t *math.pi/180 for t in tor]
        result = [0.0,0.0,0.0]
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nt,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,npol,axis = 0)
        result =np.expand_dims(result,axis=0)
        result = np.repeat(result,nr,axis = 0)
        
        for i in range(nr):
            for j in range(npol):
                for k in range(nt):
                    result[i,j,k,0] = rad[k][j*nr+i]
                    result[i,j,k,1] = pol[k][j*nr+i]
                    result[i,j,k,2] = tor[k]
        
        angles_r, angles_tht = grid_analyze.non_orthogonality(result)
        uneven_r, uneven_tht = grid_analyze.unevenness(result)
        skewness_r, skewness_tht = grid_analyze.skewness(result)
        Rangles.append(np.mean(angles_r))
        Pangles.append(np.mean(angles_tht))
        Rskew.append(np.mean(skewness_r))
        Pskew.append(np.mean(skewness_tht))
        Runeven.append(np.mean(uneven_r))
        Puneven.append(np.mean(uneven_tht))
	
plot1 = plt.figure(1)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Unorthogonality Rad'
plt.title(title)
sc1 = plt.scatter(lr, lp, s=20, c=Rangles, cmap='plasma')
plt.colorbar(sc1)
plt.savefig(title+'.png')

plot2 = plt.figure(2)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Unorthogonality Pol'
plt.title(title)
sc2 = plt.scatter(lr, lp, s=20, c=Pangles, cmap='plasma')
plt.colorbar(sc2)
plt.savefig(title+'.png')

plot3 = plt.figure(3)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Skewness Rad'
plt.title(title)
sc3 = plt.scatter(lr, lp, s=20, c=Rskew, cmap='plasma')
plt.colorbar(sc3)
plt.savefig(title+'.png')

plot4 = plt.figure(4)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Skewness Pol'
plt.title(title)
sc4 = plt.scatter(lr, lp, s=20, c=Pskew, cmap='plasma')
plt.colorbar(sc4)
plt.savefig(title+'.png')

plot5 = plt.figure(5)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Unevenness Rad'
plt.title(title)
sc5 = plt.scatter(lr, lp, s=20, c=Runeven, cmap='plasma')
plt.colorbar(sc5)
plt.savefig(title+'.png')

plot6 = plt.figure(6)
plt.xlabel('Nb Radial surfaces')
plt.ylabel('Nb Poloidal Surfaces')
title = 'Unevenness Pol'
plt.title(title)
sc6 = plt.scatter(lr, lp, s=20, c=Puneven, cmap='plasma')
plt.colorbar(sc6)
plt.savefig(title+'.png')

plt.show()
