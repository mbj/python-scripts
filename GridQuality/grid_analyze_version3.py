import numpy as np
from quick_g import quick_g_plot

# not whole loop, only tenth -> phi doesnt loop    
def yield_grid_cells(g, r_lahead=0, tht_lahead=1, phi_lahead=0):
    g = g[:,:-1] # remove double tht=0,2pi values
    # implement general looping edges g[r,tht,phi,c]; tht loops, r, phi not
    ors, othts, ophis, _ = g.shape  # original xxx size
    g = np.pad(g, [(0,0), (0,tht_lahead), (0,0), (0,0)], mode='wrap')
    
    # create smaller output array
    h = np.zeros((ors-r_lahead, othts, ophis-phi_lahead,
                  1+r_lahead, tht_lahead+1, phi_lahead+1, 3))
    
    # iterate over all parameter coordinates
    for cr in range(ors-r_lahead):
        for ctht in range(othts): #tht loops
            for cphi in range(ophis-phi_lahead):
                h[cr, ctht, cphi] = g[cr:cr+r_lahead+1,
                                      ctht:ctht+tht_lahead+1,
                                      cphi:cphi+phi_lahead+1]    
    return h
     
def _cell_centers(g, x, direction):
    mid = np.mean(x[0:2,0:2], axis=(0,1))[0]
    if direction == "r":
        mid_af = np.mean(x[1:3,0:2], axis=(0,1))[0]
    else:
        mid_af = np.mean(x[0:2,1:3], axis=(0,1))[0]
    return mid, mid_af
         
def _facemid(g, x, direction):
    if direction == "r":
        facemid_af = np.mean(x[1,0:2], axis=0)[0]
    else:
        facemid_af = np.mean(x[0:2,1], axis=0)[0]
    return facemid_af    
        
def volume(g): # APPROXIMATION
    wv = yield_grid_cells(g, 1, 1, 1)  # window view
    # calculate coord differences 
    r = wv[...,0,0,0,0]
    dr = np.diff(wv[...,:,0,0,:], axis=-2)[...,0,:]
    dz = np.diff(wv[...,0,:,0,:], axis=-2)[...,0,:]
    drphi = np.diff(wv[...,0,0,:,:], axis=-2)[...,0,:]
    # change phi angle difference to space difference by multiplying with r
    drphi *= r.reshape(r.shape + (1,)) # dphi * r
    # calculate volume from column vector determinant
    vectors = np.empty(dr.shape + (3,))
    vectors[...,0], vectors[...,1], vectors[...,2] = dr, dz, drphi
    vols = np.linalg.det(vectors)
    return vols
      
def _non_orthogonality(g, direction):
    assert direction == "r" or direction == "tht"
    
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # angles on faces
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # direction of edges 
                t_af = x[1,1]-x[1,0] if direction == "r" else x[1,1]-x[0,1]
                tn_af = t_af/np.linalg.norm(t_af)
                # angle between edge direction and midpoint direction
                af[r,tht,phi] = np.abs(np.arcsin(np.dot(t_af, v_af)))
    return af
    
def non_orthogonality(g):
    a_r = _non_orthogonality(g, "r")
    a_tht = _non_orthogonality(g, "tht")
    a_r *= 180/np.pi; a_tht *= 180/np.pi
    return a_r, a_tht

def _unevenness(g, direction):
    assert direction == "r" or direction == "tht"
    
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # unevenness on faces, also named a
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,:2]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # cell midpoint line midpoint m_f
                linemid_af = np.mean([mid, mid_af], axis=0)
                # cell corner connection line (edge) midpoint c_f
                facemid_af = _facemid(g, x, direction)
                # line distance to point
                ld_af = np.linalg.norm(np.cross(v_af, facemid_af-mid_af))
                # rotated cell mitpoint dir by 90 deg
                n_af = np.array([-v_af[1], v_af[0]])
                # check if needed to add or remove
                f_af = np.sign(np.dot(n_af, facemid_af-mid_af))
                # move corner connection line (edge) midpoint on line c_f'
                proj_facemid_af = facemid_af - f_af*ld_af*n_af
                # distance of c_f' and m_f
                num_dist_af = np.linalg.norm(proj_facemid_af-linemid_af)
                # distance between two cell midpoints 
                den_dist_af= np.linalg.norm(mid_af - mid)
                # measure of unevenness
                af[r,tht,phi] = num_dist_af/den_dist_af
    return af

def unevenness(g):
    a_r = _unevenness(g, "r")
    a_tht = _unevenness(g, "tht")
    return a_r, a_tht

def _skewness(g, direction):
    assert direction == "r" or direction == "tht"
    
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # unevenness on faces, also named a
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,:2]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # cell corner connection line (edge) midpoint c_f
                facemid_af = _facemid(g, x, direction)
                # line distance to point == ||c_f' - c_f||
                ld_af = np.abs(np.cross(v_af, facemid_af-mid_af))
                # distance between two cell midpoints 
                den_dist_af = np.linalg.norm(mid_af - mid)
                # measure of skewness
                af[r,tht,phi] = ld_af/den_dist_af
    return af

def skewness(g):
    a_r = _skewness(g, "r")
    a_tht = _skewness(g, "tht")            
    return a_r, a_tht

# THIS WORKS NOW, but not before !!!!
def convex(g):
    wv = yield_grid_cells(g, 1, 1, 0)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # skewness on r and tht faces, also named a
    is_convex = np.zeros((rs, thts, phis), dtype=int)
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,0,:2]
                # from point, to point, *other point indices to check against
                # its just looping through the subarray, feel free to improve
                indices = [[(0,0), (1,0), (1,1), (0,1)],
                           [(1,0), (1,1), (0,1), (0,0)],
                           [(1,1), (0,1), (0,0), (1,0)],
                           [(0,1), (0,0), (1,0), (1,1)]]
                for (of, to, A, B) in indices:
                    di = x[to] - x[of] # direction of line
                    # check if A and B on same side of line. If so, then 
                    # the crossproduct with the direction has the same sign
                    vA = np.sign(np.cross(di, x[A]-x[of]))
                    vB = np.sign(np.cross(di, x[B]-x[of]))
                    is_convex[r,tht,phi] += vA + vB
    # there are 8 relations per cell that are tested (and added) if all are
    # true, the result will be 8. The smaller the number the worse the cell
    is_convex = ( is_convex == 8 )
    return is_convex

def fast_convex(g):
    wv = yield_grid_cells(g, 1, 1, 0)  # window view
    wv =  wv[...,0,:2] # ignore phi component in cell and coord
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # the check works in the way, that the cell has 4 edges and for
    # each edge (going counterclockwise), the other cell corners 
    # need to be on the left side of the edge for the cell to be convex
    # cell edge directions along parametrisation in r and tht
    dir_r, dir_tht  = np.diff(wv, axis=3), np.diff(wv, axis=4)
    # we need to invert half the edge- (2nd in r and 1st in tht) 
    # direction vectors so they point conterclockwise
    # views into dir_r, d_tht for easy access
    dir_r0, dir_r1 = dir_r[...,0,0,:], dir_r[...,0,1,:] 
    dir_tht0, dir_tht1 = dir_tht[...,0,0,:], dir_tht[...,1,0,:]
    dir_r1 *= -1; dir_tht0 *= -1
    # now we need to check if the vectors in order of
    # r[0], tht[1], (-)r[1], (-)tht[0] are always turning left
    # from the last vector (between 0° and 180°).
    # cell convex IFF above true for all four edges
    cross_values = np.empty((rs, thts, phis, 4))
    cross_values[...,0] = np.cross(dir_r0, dir_tht1)
    cross_values[...,1] = np.cross(dir_tht1, dir_r1)
    cross_values[...,2] = np.cross(dir_r1, dir_tht0)
    cross_values[...,3] = np.cross(dir_tht0, dir_r0)
    # we can take the min > 0 to see if all of them are > 0
    is_convex = cross_values.min(axis=-1) > 0
    return is_convex
    
def _stats(arr, pprint):
    arr = arr.flatten()
    mean = np.mean(arr)
    median = np.median(arr)
    std = np.std(arr)
    minv = np.min(arr)
    maxv = np.max(arr)
    
    if pprint:
        print(f"""Stat values for {pprint}: 
        µ={mean:.3e}, σ={std:.3e},
        min:{minv:.3e}, max:{maxv:.3e}, med:{median:.3e}""" )
    return {"mean":mean, "std":std, "min":minv, "max":maxv, "median":median}

# return (mean, std, minv, maxv, median) for every argument
def stats(*arrs, combine=True, pprint=False):
    if combine:
        arrs = np.concatenate(arrs)
        return _stats(arrs, pprint)
    else:
        stat_outputs = []
        for i, arr in enumerate(arrs):
            stat_outputs.append(_stats(arr, pprint + f" {i}"))
        return stat_outputs

if __name__ == "__main__":
    try:
        g = np.load("g.npy")
        # g = np.load("g_kaputt.npy")
    except:
        print("Need to generate grid array 'g.npy' with another program")
        raise
    
    # quick_g_plot(g, phi=0)
    # nonc = nonconvex(g) # TODO  1 == good, 0 == nonconvex, -1 == weird
    
    # volumes = volume(g)
    angles_r, angles_tht = non_orthogonality(g)
    # uneven_r, uneven_tht = unevenness(g)
    # skewness_r, skewness_tht = skewness(g)
    # print(f"{np.sum(volumes):.3E} <- Whole volume in m^3")
    # print(f"{np.min(volumes):.3E}, {np.max(volumes):.3E} <- Min, Max cell volume in m^3")
    # print(f"{np.mean(volumes):.3E}, {np.std(volumes):.3E} <- Mean, Std of cell volume in m^3")
    
