import numpy as np
from matplotlib import pyplot as plt

def quick_g_plot(g, phi=-1, title=None):
    R, z, _ = np.transpose(g[:,:,phi,:], (2,0,1))
    fig, ax = plt.subplots(figsize=(8,8))
    ax.set_aspect("equal")
    ax.plot(R,z, c="blue", lw=0.3)
    ax.plot(R.T,z.T, c="red", lw=0.3)
    if title:
        ax.set_title(title)
    elif title is None:
        ax.set_title(f"Crossection at the {phi}. slice")
    fig.tight_layout()
