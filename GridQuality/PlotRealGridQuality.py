import grid_analyze_version4 as grid_analyze
import sys
import numpy as np
import matplotlib.pyplot as plt
from MeshFunctions import *
import math
from gg import GG

metric = int(sys.argv[1])
tpos = int(sys.argv[2])
metrics = ['Radial Skewness', 'Poloidal Skewness', 'Radial Unevenness', 'Poloidal Unevenness', 'Radial Unorthogonality', 'Poloidal Unorthogonality'] 
print(metric,metrics[metric])

grid = GG()
dat = grid.read_mesh3d(fn='GEOMETRY_3D_DATA')

R = dat.R
z = dat.z
p = dat.p

print(z.shape)
npol = R.shape[0]
nr = R.shape[1]
nt = R.shape[2]
#tor = [t *math.pi/180 for t in tor]
result = [0.0,0.0,0.0]
result =np.expand_dims(result,axis=0)
result = np.repeat(result,nt,axis = 0)
result =np.expand_dims(result,axis=0)
result = np.repeat(result,npol,axis = 0)
result =np.expand_dims(result,axis=0)
result = np.repeat(result,nr,axis = 0)
        
for i in range(nr):
    for j in range(npol):
        for k in range(nt):
            result[i,j,k,0] = R[j,i,k]
            result[i,j,k,1] = z[j,i,k]
            result[i,j,k,2] = p[k]
angles_r, angles_tht = grid_analyze.non_orthogonality(result)
uneven_r, uneven_tht = grid_analyze.unevenness(result)
skewness_r, skewness_tht = grid_analyze.skewness(result)

results = [skewness_r, skewness_tht,uneven_r,uneven_tht,angles_r,angles_tht]
print('shape',angles_r.shape)

res = results[metric]
angles_r2D = np.around(res[:,:,1],9)
nr=nr-1



pos = result[:,:,tpos,0:2]
#pos = positions.reshape((npol*nr,-1))
centerpos = np.zeros(((npol-1)*(nr-1),2))
anglesR = np.zeros(((npol-1)*(nr-1),1))

for i in range(npol-1):
    for j in range(nr-1):
        if i == npol-1:
            ind=0
        else:
            ind=i+1
        centerpos[i*(nr-1)+j,0] = (pos[j,i,0]+pos[j+1,i,0]+pos[j,ind,0]+pos[j+1,ind,0])/4
        centerpos[i*(nr-1)+j,1] = (pos[j,i,1]+pos[j+1,i,1]+pos[j,ind,1]+pos[j+1,ind,1])/4
        anglesR[i*(nr-1)+j,0] = angles_r2D[j,i]

anglesR.reshape(anglesR.shape[0])
res = [r for r in anglesR[:,0]]
y = [r for r in centerpos[:,1]]
x = [r for r in centerpos[:,0]]

#Plot the mesh
grid = GG()
dat = grid.read_mesh3d(fn='GEOMETRY_3D_DATA')
grid.plot_mesh3d(dat,tpos)

plt.xlabel('Radial Position')
plt.ylabel('Vertical Position')
title = metrics[metric]
plt.title(title)
sc1 = plt.scatter(x,y, s=20, c=res, cmap='plasma')
plt.colorbar(sc1)
plt.savefig(title+'Tor'+str(tpos)+'.pdf')

plot2 = plt.figure(2)
plt.xlabel('Radial Position')
plt.ylabel('Vertical Position')
title = metrics[metric]
plt.title(title)
sc1 = plt.scatter(x,y, s=20, c=res, cmap='plasma')
plt.colorbar(sc1)
plt.savefig(title+'OnlyQuality'+'Tor'+str(tpos)+'.pdf')

plt.show()
