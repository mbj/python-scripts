'''
import gg; reload(gg); g = gg.GG()
import Kisslinger as kis; reload(kis)
flx = g.read_flux(); g.plot_flux(flx)
lc,lm,li = kis.load_components()
msh = g.read_mesh2d();g.plot_mesh2d(msh); kis.plot_components(lc,phi=0)
grd = g.read_mesh3d(); phi = g.plot_mesh3d(grd,i=0,ridx=[15,20,30,40]); kis.plot_components(lc,phi)  
'''
import numpy as np
from matplotlib import pyplot as plt
import utils.data as data
import Kisslinger

class GG():

    def __init__(self):
        pass
        return

    def read_trace(self,fn='out'):
        dat = np.loadtxt(fn)
        return dat

    def plot_trace(self,dat,fig=None):
         if fig == None:
            fig = plt.figure()
         plt.plot(dat[:,0],dat[:,1],'+',ms=0.5)
         plt.show(block=False)
         return fig

    def read_flux(self,fn='SURFS_OUT'):
        fp = open(fn,'br')
        line = fp.readline().split()
        ns = int(line[0])

        x = np.zeros((ns,1000,2))
        y = np.zeros((ns,1000,2))
        npp = np.zeros((ns,2))
        npp = np.array(npp,dtype=np.int)

        for i in range(ns):
            line = fp.readline().split()
            npp[i,0],npp[i,1] = int(line[0]),int(line[1])
            for j in range(npp[i,0]):
                line = fp.readline().split()
#                for k in range(npp[i,1]):
#                    x[i,j*npp[i,1]+k] = line[0+2*k]
#                    y[i,j*npp[i,1]+k] = line[1+2*k]
                x[i,j,0] = line[0]
                y[i,j,0] = line[1]
                x[i,j,1] = line[2]
                y[i,j,1] = line[3]

        fp.close()

        dat= data.data()
        dat.x = x
        dat.y = y
        dat.ns = ns
        dat.npp = npp
        return dat

    def plot_flux(self,dat,idx=0,fig=None,c=None):
        if fig == None:
            fig = plt.figure()
        for i in range(dat.ns):
            plt.plot(dat.x[i,0:dat.npp[i,0],idx],dat.y[i,0:dat.npp[i,0],idx],'+',c=c)
        plt.show(block = False)
        return fig

    def read_mesh2d(self,fn='MESH_2D'):
        fp = open(fn,'r')
        line = fp.readline().split()
        nx,ny,nz = int(line[0]),int(line[1]),int(line[2])

        line = fp.readline().split()
        tmp = float(line[0])

        npp = nx*ny*nz
        x = np.zeros(npp)
        y = np.zeros(npp)
        
        idx = 0
        tx = []
        while idx < npp:
            line = fp.readline().split()
            idx += len(line)
            for item in line:
                if not('E' in item): item = 0.
                item = float(item)
                if item > 1E3 or item < -1E3: item = 0.
                tx.append(float(item))

        idx = 0
        ty = []
        while idx < npp:
            line = fp.readline().split()
            idx += len(line)
            for item in line:
                if not('E' in item): item = 0.
                item = float(item)
                if item > 1E3 or item < -1E3: item = 0.
                ty.append(float(item))


        fp.close()

        dat= data.data()
        dat.x = np.array(tx)
        dat.y = np.array(ty)
        dat.npp = npp
        dat.nx,dat.ny,dat.nz = nx,ny,nz
        return dat

    def plot_mesh2d(self,dat,color='green',fig=None):
        if fig == None:
            fig = plt.figure(1)
        x = dat.x.reshape((dat.ny,dat.nx))
        y = dat.y.reshape((dat.ny,dat.nx))
        for i in range(dat.nx):
            plt.plot(x[:,i],y[:,i],'-',c=color,lw=0.2)
#        plt.show(block=False)
        return fig

    def read_mesh3d(self,fn='GRID_3D_DATA'):
        ncol = 6
        fp = open(fn,'r')
        line = fp.readline().split()
        nx,ny,nz = int(line[0]),int(line[1]),int(line[2])
        npp = nx*ny

        R = np.zeros((ny,nx,nz))
        z = np.zeros((ny,nx,nz))
        p = np.zeros(nz)
        for iz in range(nz):
            p[iz] = float(fp.readline())
            #print(p[iz])
            tmp = []
            for i in range(int(np.ceil(npp/ncol))):
                line = fp.readline().split()
                tmp.extend([float(item) for item in line])
            R[:,:,iz] = np.array(tmp,dtype=np.float).reshape(ny,nx)
            tmp = []
            for i in range(int(np.ceil(npp/ncol))):
                line = fp.readline().split()
                tmp.extend([float(item) for item in line])
            z[:,:,iz] = np.array(tmp,dtype=np.float).reshape(ny,nx)
        dat = data.data()
        dat.R = R 
        dat.z = z
        dat.p = p
        return dat

    def plot_mesh3d(self,dat,i,pidx=[],ridx=[]):
        nx, ny = dat.R[:,:,0].shape
        for j in range(nx):
            plt.plot(dat.R[j,:,i],dat.z[j,:,i],'b',lw=0.2, color='green')
        for j in range(ny):
            plt.plot(dat.R[:,j,i],dat.z[:,j,i],'b',lw=0.2,color='green')
        for j in pidx:
            plt.plot(dat.R[j,:,i],dat.z[j,:,i],'r',lw=0.5,color='green')
        for j in ridx:
            plt.plot(dat.R[:,j,i],dat.z[:,j,i],'r',lw=0.5,color='green')
        #plt.plot(dat.R[:,:,i],dat.z[:,:,i],'b+')
#        plt.show(block=True)
        print('Phi = %.2f' %(dat.p[i]))
        return dat.p[i]

    def plot_grid_mayavi(self,grd,idx=10,typ=0,\
                         rep='wireframe',opacity=0.99,color=(1,1,1),\
                         fig=None):
      degtorad = np.pi/180.
      from mayavi import mlab
      import utils.geom as geom
      shp = grd.R.shape
      nphi = shp[2]
      phi = np.ones(shp)
      for i in range(nphi):
          phi[:,:,i] = grd.p[i]*degtorad
      x,y,z = geom.rzptoxyz(grd.R.flatten(),grd.z.flatten(),phi.flatten())
      x = x.reshape(shp)
      y = y.reshape(shp)
      z = z.reshape(shp)
      print(x.shape)

      if fig is None: fig = mlab.figure()
      #Poloidal Surface (tor. Cut)
      if typ == 0:
        mlab.mesh(x[:,:,idx],y[:,:,idx],z[:,:,idx],\
                  representation=rep,color=color,\
                  opacity=opacity)
      #Toroidal surface (rad. cut)
      elif typ == 1:
        mlab.mesh(x[:,idx,:],y[:,idx,:],z[:,idx,:],\
                  representation=rep,color=color,\
                  opacity=opacity)
      #Tor. surfaces (pol. cuts)
      elif typ == 2:
        mlab.surf(x[idx,:,:],y[idx,:,:],z[idx,:,:],\
                  representation=rep,color=color,\
                  opacity=opacity)
      return fig

    def read_intersections(self):
      fp = open('PLATES_MAG')
      zone = []
      target = []
      idxp = []
      nidx = []
      idx = []
      i = 0
      while True:
        line =  fp.readline()
        i += 1
        print(i)
        line = line.split()
        zone.append(int(line[0]))
        target.append(int(line[1]))
        idxp.append(int(line[2]))
        nidx.append(int(line[3]))
        #Doesn't work due to line break after 8 entries
        tmp = [int(item) for item in line[4:]]
        if nidx[-1] > 8:
          line = fp.readline().split()
          for item in line:
            tmp.append(item)
        idx.append(tmp)        
      dat = data.data()
      dat.zone = zone
      dat.target = target
      dat.idxp = idxp
      dat.nidx = nidx
      dat.idx = idx
      dat.num = len(zone)
      return dat
