#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.%j
#SBATCH -e ./tjob.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J GeometriesforResolutionStudy
# Queue (Partition):
#SBATCH --partition=short
# Don't specify 'SBATCH --nodes' !
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem=60G

#SBATCH --mail-type=none
#SBATCH --mail-user=user@mbj.mpg.de
#
# Wall clock limit:
#SBATCH --time=00:10:00

#python ResolutionStudyGeometries.py

echo " Running Cases "

cd ../../data-for-drift-computations/ResolutionStudies/

for d in */ ; do
    cd ./$d
    echo "$d"
    echo "$(pwd)"
    declare -a DIRS=()
    for dir in */ ; do
	DIRS[${#DIRS[@]}]="$dir"
    done
    echo "${DIRS[*]}"
    for case in 1 2 ; do
        echo "Doing computations for description $case"
	python ../../../python-scripts/Scripts_Matthieu/ErrorAnalysis.py $case ${DIRS[*]}
    done
    cd ..
done

echo "done all"
    
