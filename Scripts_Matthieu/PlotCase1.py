from mpl_toolkits import mplot3d
import numpy as nump
import matplotlib
import matplotlib.pyplot as plt
from MeshFunctions import *
import math
import AnalyticalPrescriptions
from driftFunctions import *


# General Info about the case to plot
nt =1; nr = 10; np = 10
Rmajor = 500
rminor = 100
Trange = 36
# Create the vertices
[tor,rad,hor] = create_toroidal_vertices(nt,nr,np,Trange,Rmajor,rminor)
[xlines_pol,xlines_rad,xlines_tor,ylines_pol,ylines_rad,ylines_tor,zlines_pol,zlines_rad,zlines_tor] = plotMesh(tor,rad,hor,nt,nr,np,None,show=True)


def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    R = 0.01*R
    bval = math.exp(-R) # R in cm
    return bval
x,z,Bf = plot2DContour(100,100,Rmajor-rminor,Rmajor+rminor,-rminor,rminor,Rmajor,rminor,b_field_description,'','','',False)

[xdata,ydata,zdata] = plotGridPoints(tor,rad,hor,show = False)
Bdata = []
for i in range(len(xdata)):
    Bdata.append(b_field_description(xdata[i],zdata[i],0,Rmajor,rminor))

# Evaluate the electric field at all points:
def potential(r,z,theta,Rmaor,rminor):
    R = math.sqrt((r - Rmajor) ** 2 + z ** 2)
    return Rmajor/R
def e_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)
    # Potential as 1/r => Efield: -1/r^2
    er = -(r-Rmajor)/(((r-Rmajor)**2+z**2))**(3/2)*Rmajor
    etheta = 0
    ez = -z/Rmajor/(((r-Rmajor)**2+z**2)/Rmajor)**(3/2)*Rmajor
    return [er, etheta, ez]

def gradB(r,z,theta,Rmajor,rminor):
    gbr = -(r-Rmajor)/(Rmajor*math.sqrt(z**2+(r-Rmajor)**2))*math.exp(-math.sqrt(z**2+(r-Rmajor)**2)/Rmajor)
    gbz = -z/(Rmajor*math.sqrt(z**2+(r-Rmajor)**2))*math.exp(-math.sqrt(z**2+(r-Rmajor)**2)/Rmajor)
    return [gbr, 0, gbz]

def bvector(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    R = R/Rmajor
    btheta = math.exp(-R) # R in cm
    return [0,btheta,0]

# Get two points, where the drifts (exb or gradb) will be computed and the vector plotted
p1 = [xlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)],ylines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)],zlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)]]
p2 = [xlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)],ylines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)],zlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)]]
r1 = math.sqrt(p1[0]**2+p1[1]**2)
theta1 = math.atan2(p1[1],p1[0])
z1 = p1[2]
r2 = math.sqrt(p2[0]**2+p2[1]**2)
theta2 = math.atan2(p2[1],p2[0])
z2 = p2[2]

E1 = e_field_description(r1,z1,theta1,Rmajor,rminor)
B1 = bvector(r1,z1,theta1,Rmajor,rminor)
B2 = bvector(r2,z2,theta2,Rmajor,rminor)
gradB2 = gradB(r2,z2,theta2,Rmajor,rminor)
vexb = ExBdrift(E1,B1)
vgradb = gradBdrift(B2,gradB2)
# Plot only the first outer surface and 2 toroidal lines
fig = plt.figure()
ax = plt.axes(projection='3d')
scale = 100
E1 = cyltocart(E1,p1)
B1 = cyltocart(B1,p1)
B2 = cyltocart(B2,p2)
gradB2 = cyltocart(gradB2,p2)
E1 = 10*scale*nump.array(E1)
B1 = scale*nump.array(B1)
B2 = scale*nump.array(B2)
gradB2 = 100*scale*nump.array(gradB2)
vexb = 10*scale*vexb
vgradb = scale*vgradb

ax.quiver(p1[0],p1[1],p1[2],E1[0], E1[1],E1[2],color = 'yellow',label='Electric Field Vector')
ax.quiver(p1[0],p1[1],p1[2],B1[0], B1[1],B1[2],color = 'blue',label = 'Magnetic Field Vector')
ax.quiver(p1[0],p1[1],p1[2],vexb[0],vexb[1],vexb[2],color = 'red',label = 'E x B Drift Velocity')
ax.quiver(p2[0],p2[1],p2[2],gradB2[0], gradB2[1],gradB2[2],color = 'black',label = 'Magnetic Field Strength Gradient')
ax.quiver(p2[0],p2[1],p2[2],B2[0], B2[1],B2[2],color = 'blue')
ax.quiver(p2[0],p2[1],p2[2],vgradb[0],vgradb[1],vgradb[2],color = 'purple',label = 'Grad B Velocity')
ax.plot(xlines_pol[nr-1], ylines_pol[nr-1], zlines_pol[nr-1],color='green')
ax.plot(xlines_pol[(nt)*nr-1], ylines_pol[(nt)*nr-1], zlines_pol[(nt)*nr-1],color='green')
ax.plot(xlines_tor[math.floor(np/4)*nr-1], ylines_tor[math.floor(np/4)*nr-1], zlines_tor[math.floor(np/4)*nr-1],color='green')
ax.plot(xlines_tor[math.floor(np/2)*nr-1], ylines_tor[math.floor(np/2)*nr-1], zlines_tor[math.floor(np/2)*nr-1],color='green')
#ax.contourf(z,x,Bf,100,zdir='z')
ax.set_xlabel('X [cm]')
ax.set_ylabel('Y [cm]')
ax.set_zlabel('Z [cm]')
ax.set_title('Test Case 1')
ax.legend()
plt.show()

# Colorplot of magnetic field and potential
# Plot 2d contour of Bfield
ns = 100
plot2DContour(ns,ns,Rmajor-rminor,Rmajor+rminor,-rminor,rminor,Rmajor,rminor,b_field_description,'X [cm]', 'Y [cm]','Magnetic field strength [T]',True)
plot2DContour(ns,ns,Rmajor-rminor,Rmajor+rminor,-rminor,rminor,Rmajor,rminor,AnalyticalPrescriptions.potential_description3,'X [cm]', 'Y [cm]','Potential [V]',True)