# Functions to extract the information from the fortran output and compare with analytical results

import Constants
import math
import Utilities
import pandas as pd
import numpy as np
import ast

def extractValues(positions,gbpositions, drifts, efield, gradb, drgradb, bcells,bnodes, edescr, bdescr, gradbdescr, ExB, gradBdrift, ggcorr,potential, pdescr, Rmajor):
    pos = []
    gbpos = []
    enum = []
    denum = []
    gnum = []
    dgnum = []
    enumperp = []
    denumperp = []
    gnumperp = []
    dgnumperp = []
    ea = []
    ba = []
    dea = []
    dga = []
    ga = []
    pa = []
    bound = []
    boundnodes = []
    ggcorrnum = []
    pot = []
    print(len(drifts))
    print(len(efield))
    print(len(ggcorr))
    for i in range(len(drifts)):
        # Convert to float values to allow computations and interpretation
        den = [float(x) for x in drifts[i].split()]
        dpn = [float(x) for x in positions[i].split()]
        en = [float(x) for x in efield[i].split()]
        dbn = [float(x) for x in drgradb[i].split()]
        bnd = float(bcells[i])
        gg = float(ggcorr[i])
        p = float(potential[i])

        # filter our boundary cells => These have zero field and drift and are not useful for interpretation!
	# quick fix with nan
        if  bnd != 1 and not math.isnan(en[0]) and not math.isnan(den[0]):# and (abs(max(en))) <300 and abs(min(en))<300: 
            denum.append(den)
            pos.append(dpn)
            enum.append(en)
            dgnum.append(dbn)
            # Compute analytical efield and drifts at these positions
            efa = edescr(dpn[0], dpn[2], dpn[1], Rmajor)
            bfa = bdescr(dpn[0], dpn[2], dpn[1],Rmajor, Constants.Rminor)
            gfa = gradbdescr(dpn[0], dpn[2], dpn[1], Rmajor, Constants.Rminor)
            dgfa = gradBdrift(bfa,gfa)
            pan = pdescr(dpn[0],dpn[2],dpn[1],Rmajor, Constants.Rminor)
            pa.append(pan)
            dga.append(dgfa.tolist())
            ea.append(efa)
            ba.append(bfa)
            ggcorrnum.append(gg)
            pot.append(p)
            dea.append(ExB(efa, bfa).tolist())
            denumperp.append(transformRadPerptoCyl(denum[-1],pos[-1], Rmajor))
            enumperp.append(transformRadPerptoCyl(enum[-1],pos[-1], Rmajor))
            dgnumperp.append(transformRadPerptoCyl(dgnum[-1],pos[-1], Rmajor))
	    
    for i in range(len(gradb)):
        gbn = [float(x) for x in gradb[i].split()]
        bnd = float(bnodes[i])
        dpn = [float(x) for x in gbpositions[i].split()]
        if  bnd != 1:
            gnum.append(gbn)
            gfa = gradbdescr(dpn[0], dpn[2], dpn[1], Rmajor, Constants.Rminor)
            ga.append(gfa)
            gbpos.append(dpn)
            gnumperp.append(transformRadPerptoCyl(gnum[-1],gbpos[-1],Rmajor))
    return [pos,gbpos, enum, denum,gnum,dgnum, enumperp,denumperp, gnumperp ,dgnumperp,ea , ba, dea, ga,dga, ggcorrnum, pot, pa]

def extractVectorInfo(numeric,analytical):
    res_mag = []
    an_mag = []
    er_mag = []
    er_vec = []
    er_rel_vec = []
    er_rel_mag = []
    for i in range(len(numeric)):
        res_mag.append(math.sqrt(sum([x**2 for x in numeric[i]])))
        an_mag.append(math.sqrt(sum([x**2 for x in analytical[i]])))
        er_vec.append(Utilities.listdif(numeric[i],analytical[i]))
        er_mag.append(math.sqrt(sum([x**2 for x in er_vec[i]])))
        er_rel_vec.append([math.sqrt(x**2)/max(an_mag[i],0.000000001) for x in er_vec[i]])
        if an_mag[i] == 0:
            print('ZERO!')
        er_rel_mag.append(sum(er_rel_vec[i]))

    return [res_mag,an_mag,er_mag,er_vec,er_rel_mag,er_rel_vec]

def slice(positions,info,direction, coordinate):
    # Get a slice of the full range of information along a certain direction
    # positions are the coordinates corresponding to the info values
    # direction: between 0 and 2: along which direction to take a slice!
    # coordinate specifies which surface to keep as slice
    pos_slice = []
    info_slice = []
    index1 = direction-1 %3
    index2 = direction+1 %3
    for i in range(len(positions)):
        if positions[i][direction] in coordinate:
            pos_slice.append([positions[i][index1],positions[i][index2]])
            info_slice.append(info[i])

    return [pos_slice, info_slice]

def getAllSlices(positions,allInfo,direction,coordinate):
    pos_slice = []
    result = []
    for i in range(len(allInfo)):
        [pos_slice, info_slice] = slice(positions,allInfo[i],direction,coordinate)
        result.append(info_slice)
    result.append(pos_slice)
    return result

def getAllInfo(positions,gbpositions,drifts,efield, gradb, drgradb,bcells,bnodes,edescr,bdescr, gradbdescr, ExB, gradBdrift,GGcorr,pot,pdescr,Rmajor, name):
    [pos, gbpos, enum, denum,gnum,dgnum, enumperp,denumperp, gnumperp ,dgnumperp,ea , ba, dea, ga,dga, GGcorr, potential, potan] = extractValues(positions, gbpositions,
                        drifts, efield, gradb, drgradb, bcells,bnodes, edescr, bdescr,gradbdescr, ExB, gradBdrift, GGcorr, pot,pdescr,Rmajor)

    [deres_mag, dean_mag, deer_mag, deer_vec, deer_rel_mag, deer_rel_vec] =  extractVectorInfo(denumperp,dea)
    [eres_mag, ean_mag, eer_mag, eer_vec, eer_rel_mag, eer_rel_vec] = extractVectorInfo(enumperp, ea)
    [gres_mag, gan_mag, ger_mag, ger_vec, ger_rel_mag, ger_rel_vec] = extractVectorInfo(gnumperp, ga)
    [dgres_mag, dgan_mag, dger_mag, dger_vec, dger_rel_mag, dger_rel_vec] = extractVectorInfo(dgnumperp, dga)
#    print('an',dgan_mag)
 #   print('res', dgres_mag)
    
    potdif = Utilities.listdif(potential,potan)
    InfoDict = {}
    InfoDictGB = {}
    keys = ['edrifts', 'efield','edrifts_perp','efield_perp' ,'ef_mag', 'ef_an_mag','ef_an', 'edrift_mag', 'edrift_an_mag', 'ef_vec_er', 'ef_mag_er',
            'edrift_vec_er', 'edrift_er_mag','efield_er_rel_mag', 'efield_er_rel_vec', 'edrift_er_rel_mag', 'edrift_er_rel_vec',
            'gdrifts', 'gdrifts_perp', 'gdrift_mag','gdrift_an_mag',
            'gdrift_vec_er','gdrift_er_mag','gdrift_er_rel_mag', 'gb_er_rel_vec', 'GGcorr','potential','potan','potdif','pos']
    keysgb = ['gradb','gradb_perp', 'gb_mag','gb_an_mag','gb_vec_er', 'gb_mag_er','gb_er_rel_mag','gb_er_rel_vec','gb_pos']
    values = [denum,enum,denumperp,enumperp,eres_mag,ean_mag,ea,deres_mag,dean_mag,eer_vec,eer_mag,deer_vec,deer_mag,eer_rel_mag,eer_rel_vec,deer_rel_mag,deer_rel_vec,
              dgnum,dgnumperp,dgres_mag,dgan_mag,dger_vec,dger_mag,dger_rel_mag,dger_rel_vec,GGcorr,potential,potan,potdif,pos]
    valuesgb = [gnum,gnumperp,gres_mag,gan_mag,ger_vec,ger_mag,ger_rel_mag,ger_rel_vec,gbpos]
    for i in range(len(values)):
        InfoDict[keys[i]] = values[i]
    for j in range(len(valuesgb)):
        InfoDictGB[keysgb[j]] = valuesgb[j]
    pandasDF = pd.DataFrame.from_dict(data = InfoDict)
    pandasDF.to_csv('AllInfo'+name+'.csv', header=True)
    pandasDF = pd.DataFrame.from_dict(data = InfoDictGB)
    pandasDF.to_csv('AllInfoGB'+name+'.csv', header=True)
    return [InfoDict,InfoDictGB]


def readText(filepath):
    file = open(filepath,'r')
    result = []
    for line in file:
        result.append(line)
    return result
    
    
def transformCyltoRadPerp(v,pos, Rmajor):
    result = []
    phi = math.atan2(pos[2],pos[0]-Rmajor)
    c = math.cos(phi)
    s = math.sin(phi)
    result.append(v[0]*c+v[2]*s)
    result.append(v[1])
    result.append(v[2]*c-v[0]*s)
    return result
    
def readAllInfo(method,descr,noise,folder):
    result = {}
    print(folder+'AllInfo'+method+'Descr'+descr+'Noise'+noise+'.csv')
    df = pd.read_csv(folder+'AllInfo'+method+'Descr'+descr+'Noise'+noise+'.csv')
    keys = list(df.columns)
    for i in range(1,len(keys)):
        series = df.loc[:,keys[i]].to_list()
        N = len(series)
        res = np.array([None]*N)
        for j in range(N):
            if type(series[j]) is str:
                res[j] = np.array(ast.literal_eval(series[j]))
            else:
                res[j] = series[j]
        result[keys[i]] = res
# Also get grad b info
    resultGB = {}
    df = pd.read_csv(folder+'AllInfoGB'+method+'Descr'+descr+'Noise'+noise+'.csv')
    keys = list(df.columns)
    for i in range(1,len(keys)):
        series = df.loc[:,keys[i]].to_list()
        N = len(series)
        res = np.array([None]*N)
        for j in range(N):
            if type(series[j]) is str:
                res[j] = np.array(ast.literal_eval(series[j]))
            else:
                res[j] = series[j]
        resultGB[keys[i]] = res
    return [result,resultGB]

def gradientLength(gradientmags,functionvalues):
    result = []
    for i in range(len(gradientmags)):
        result.append(functionvalues[i]/(gradientmags[i]+1e-6))
    return result

def transformRadPerptoCyl(v,pos,Rmajor):
    result = []
    phi = math.atan2(pos[2],pos[0]-Rmajor)
    c = math.cos(phi)
    s = math.sin(phi)
    result.append(v[0]*c-v[2]*s)
    result.append(v[1])
    result.append(v[2]*c+v[0]*s)
    return result




