# This file contains the analytical functions describing potential, efield and magnetic field for different cases
import math
import PlottingFunctions
# Magnetic Fields
def b_field_description1(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    R = math.sqrt((r-Rmajor)**2 + z**2)
    bval = 1/R  # R in cm
    return [0,bval,0]
    
def bfieldstrength1(r,z,theta,Rmajor,Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    R = math.sqrt((r-Rmajor)**2 + z**2)
    bval = 1/R  # R in cm
    return bval

def b_field_description2(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    R = math.sqrt((r-Rmajor)**2 + z**2)
    bval = 1/R # R in cm
    return [0,bval,0]

def gradb_description1(r,z,theta,Rmajor,Rminor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    R = math.sqrt((r-Rmajor)**2 + z**2)
    gBr = -1/R**2*(r-Rmajor)/R
    gBtheta = 0
    gBz = -1/R**2*z/R
    return [gBr, gBtheta, gBz]

# Potentials
def potential_description1(r,z,theta,Rmajor,Rminor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r-Rmajor)**2+z**2)
    potval = math.exp(-D)*500
    return potval

def potential_description3(r,z,theta,Rmajor,Rminor):
    R = (r-Rmajor)/100+0.001
    potval = math.exp(-R)*100
    return potval

def potential_description2(r,z,theta,Rmajor,Rminor):
    r = (r-Rmajor)/100
    z = z/100
    potval = 100*(math.cos(2*math.atan2(z,r)) + math.sqrt(r**2+z**2))
    return potval
def potential_description7(r,z,theta,Rmajor,Rminor):
    r = (r-Rmajor)/100
    z = z/100
    potval = 100*(math.cos(2*math.atan2(z,r)))
    return potval
    

# Electric Fields
def e_descr1(r,z,theta, Rmajor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r - Rmajor) ** 2 + z ** 2)
    Er = -500*math.exp(-D)*1/D*(r-Rmajor)
    Ephi = 0
    Ez = -500*math.exp(-D)*1/D*z
    return [Er,Ephi, Ez]

def e_descr3(r,z,theta, Rmajor):
    D = (r - Rmajor)/100+0.001
    Er = -100*math.exp(-D)
    Ephi = 0
    Ez = 0
    return [Er,Ephi, Ez]
    
def e_descr5(r,z,theta, Rmajor):
    Er = 1000
    Ephi = 0
    Ez = 0
    return [Er,Ephi, Ez]


def e_descr2(r,z,theta, Rmajor):
    r = (r-Rmajor)/100
    z = z/100
    Er = 200*math.sin(2*math.atan2(z,r))*z/(z**2+r**2) +100*r/math.sqrt(r**2+z**2)
    Ephi = 0
    Ez = 200*math.sin(2*math.atan2(z,r))*-r/(z**2+r**2) + 100*z/math.sqrt(r**2+z**2)
    return [Er,Ephi,Ez]
#    return Ez

def e_descr6(r,z,theta, Rmajor):
    r = r/100
    Rmajor = 590/100
    z = z/100
    D = math.sqrt((r - Rmajor) ** 2 + z ** 2)
    Er = -500*math.exp(-D)*1/D*(r-Rmajor)
    Ephi = 0
    Ez = -500*math.exp(-D)*1/D*z
    return [Er,Ephi, Ez]
    
def e_descr7(r,z,theta, Rmajor):
    r = (r-Rmajor)/100
    z = z/100
    Er = 200*math.sin(2*math.atan2(z,r))*z/(z**2+r**2) 
    Ephi = 0
    Ez = 200*math.sin(2*math.atan2(z,r))*-r/(z**2+r**2) 
    return [Er,Ephi,Ez]

