import numpy as np
import os
import matplotlib.pyplot as plt
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import PlottingFunctions
import AnalysisFunctions
import sys
from scipy import sparse
import scipy.optimize as optimize
import math
import matplotlib
import statistics
from scipy import stats
path = os.getcwd()
sys.path.append('../../emc3-grid-generator')
from gg import GG

xnames = ['_1','_2','_3','_4','_5','_6','_7','_8','_9']#,'_10']
Tdata = []

def monexp(x,L):
    return np.exp(-x/L)


for i in range(len(xnames)):
    file = open('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI','r')
    Temptemp = [r.split() for r in file.readlines()]
    Temp = []
    for i in Temptemp:
        Temp.extend(i)
    Tempel = Temp[0:int(len(Temp)/2)]
    Tempar = np.array(Tempel, dtype=float)
    Tdata.append(Tempar)


avTemp = Tdata[0]    
for i in range(1,len(xnames)):
    print(Tdata[i][50000:50010])
    avTemp = np.add(avTemp,Tdata[i])
avTemp = avTemp*1/len(xnames)

difData = []  
normdifData = []  
variance = []
for i in range(len(xnames)):
    difTemp = np.subtract(avTemp,Tdata[i])
#    normdifTemp = np.divide(difTemp,avTemp)
    var = np.square(difTemp)
    variance.append(var)
    difData.append(difTemp)
#    normdifData.append(normdifTemp) 
    
avDif = difData[0]    
avVar = variance[0]
for i in range(1,len(xnames)):
    avDif = np.add(avDif,difData[i])
    avVar = np.add(avVar,variance[i])
avDif = avDif*1/len(xnames)
avVar = avVar*1/len(xnames)
print('average variance', statistics.mean(avVar))

filvar = filter(lambda var:var<3*median, avVar)	

plot1 = plt.figure(1)
plt.xlabel('Temperature')
plt.ylabel('Variance')
plt.title('Temperature vs Variance')
sc1 = plt.scatter(avTemp,avVar)
#plt.colorbar(sc1)


plot2 = plt.figure(2)
plt.xlabel('Av Temperature')
plt.ylabel('Temp')
plt.title('Temperature vs Av Temp')
sc1 = plt.scatter(avTemp,Tdata[1])
#plt.colorbar(sc1)
median=statistics.median(avVar)
print('median Var', median)
# plot histogram of variance
plot3 = plt.figure(3)
plt.hist(avVar,1000, density=True)
plt.title('Histogram of Variance')

# Plot variance in space
plot4 = plt.figure(4)
plt.xlabel('Radial Position')
plt.ylabel('Vertical Position')
posdata = AnalysisFunctions.readText(path+'/Output1Descr6Noise0/DRIFT_POS')
# Load position data
geoData = AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/Output5Descr6Noise0/NBS')
volumes =np.array(AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/Output3Descr3Noise0/VOLUMES'),dtype=float)

volfil=[]
varfil=[]
for i in range(len(volumes)):
      if avVar[i]<50:
          varfil.append(avVar[i])
          volfil.append(volumes[i])

plot7 = plt.figure(7)
# Creating plot
plt.hist2d(volfil, varfil, bins=1000,norm=matplotlib.colors.LogNorm())
plt.colorbar()
plt.title("Variance vs Volume")



########### Get covariance data => Try to fit correlation length model, looking at individual surfaces ##############################
# Get single error
limits = AnalysisFunctions.readText(path+'/Output1Descr6Noise0/LIMITS')
dim = math.floor(len(geoData)/5)
TS = [15]
RS=[34]
#RS=[60,61,62,63,64]
PS =  [100]
choice = 1
positions = []
difvalues=[]
variances =[]
indices=[]
vols=[]
print(len(difData))
print(len(difData[0]))
print(dim)
print(int(len(limits)/4))
for i in range(dim):
    coords =[float(x) for x in geoData[5*i+2].split()]
    limT = [float(x) for x in limits[4*i+1].split()]
    limP = [float(x) for x in limits[4*i+2].split()]
    limR = [float(x) for x in limits[4*i+3].split()]
    if choice==1:
        if limT[0] in TS and limR[0] in RS:
            positions.append(coords)
            difvalues.append([r[i] for r in difData])
            indices.append(limP[0])
    elif choice == 2:
        if limT[0] in TS and limP[0] in PS:
            positions.append(coords)
            difvalues.append([r[i] for r in difData])
    else:
        if limR[0] in RS and limP[0] in PS:
            positions.append(coords)
            difvalues.append([r[i] for r in difData])    
            indices.append(limT[0])
    
posArray = np.array(positions)
difArray = np.array(difvalues)
print(posArray)
print(difArray)
print(posArray.shape)

covmat = np.cov(difArray)
cormat = np.corrcoef(difArray)
cor1D = cormat[0]
cor1Dn = cormat[30]
cov1D = covmat[0]
print(cor1D)
distances = []
poldistances = []
polangles = []
#indices=np.linspace(0,len(cor1D),len(cor1D))
for i in range(len(cor1D)):
    dis = min(math.sqrt((positions[i][0]-positions[0][0])**2+(positions[i][2]-positions[0][2])**2+((positions[i][0]+positions[0][0])/2*abs(positions[i][1]-positions[0][1]))**2), \
    math.sqrt((positions[i][0]-positions[35][0])**2+(positions[i][2]-positions[35][2])**2+((positions[i][0]+positions[35][0])/2*abs(positions[i][1]-positions[35][1]))**2))
#    poldis = abs(math.atan2(positions[i][2],positions[i][0]-580)-math.atan2(positions[0][2],positions[0][0]-580))*(math.sqrt((positions[i][0]-580)**2+(positions[i][2])**2)+math.sqrt((positions[0][0]-580)**2+(positions[0][2])**2))/2
#    polangle = math.atan2(positions[i][2],positions[i][0]-580)
    distances.append(dis)
#    poldistances.append(poldis)
#    polangles.append(polangle)

plot=plt.figure(20)
r = [r[0] for r in positions]
z = [r[2] for r in positions]

plt.title('Spatial Distribution of Considered Points')
grid = GG()
dat = grid.read_mesh3d(fn='Inputs/GEOMETRY_3D_DATA')
grid.plot_mesh3d(dat,TS[0])
sc1 = plt.scatter(r,z,color='r')
for i in range(len(r)):
    plt.plot(r[i],z[i],"or")
xl = indices
xlab='Grid Indices'
plot5 = plt.figure(5)
sc1 = plt.scatter(xl,cov1D)
plt.title('Covariance')
plt.xlabel(xlab)
plt.ylabel('Covariance')
plot5 = plt.figure(6)
sc1 = plt.scatter(xl,cor1D)
#sc1 = plt.scatter(xl,cor1Dn)
plt.xlabel(xlab)
plt.ylabel('Correlation Ceofficient')
plt.title('Correlation with Starting Cell')
plot5 = plt.figure(8)
sc1 = plt.scatter(xl,cor1D)
plt.xlabel(xlab)
plt.ylabel('Correlation Ceofficient')
plt.title('Correlation with Starting Cell')



corlen=25
x = np.array(xl)
y=np.array(cor1D)
m, b = np.polyfit(x, y, 1)
print(m,b,'linear fit')
cv,p=stats.pearsonr(x,y)
print(cv,p)
popt,pcov=optimize.curve_fit(monexp,x,y)
print('popt',popt,'pcov',pcov)
plt.plot(x,monexp(x,*popt), label='exponential fit')
plt.plot(x,m*x+b,'--k',label='linear fit')
plt.legend()


plt.show()

#############################################################################################################

# Get single error
positions =[]
variances =[]
vols=[]
for i in range(len(posdata)):
    pos = [float(x) for x in posdata[i].split()]
    if i==0:
        torpos = pos[1]
    if pos[1] == torpos and avVar[i] < 3*median and avVar[i]>median/10:
        positions.append(pos)
        variances.append(avVar[i])
        vols.append(volumes[i])
r = [x[0] for x in positions]
z = [x[2] for x in positions]

sc1 = plt.scatter(r,z,c=variances, cmap='plasma', norm=matplotlib.colors.LogNorm())    
plt.colorbar(sc1)
plt.title('Variances filtered')

plot = plt.figure(12)
sc1 = plt.scatter(r,z,c=vols, cmap='plasma', norm=matplotlib.colors.LogNorm())    
plt.colorbar(sc1)
plt.title('Cell volumes')

 
dim = math.floor(len(geoData)/5)
print(dim)
boundary = np.empty(dim)
neighbours = np.empty((dim,6))
coords = np.empty((dim,3))
distances = np.empty((dim,6))

for i in range(dim):
    boundary[i] = geoData[5*i+1]
    neighbours[i][:] = [int(x) for x in geoData[5*i].split()]
    coords[i][:] =[float(x) for x in geoData[5*i+2].split()]
    distances[i][:] = [float(x) for x in geoData[5*i+3].split()]+[float(x) for x in geoData[5*i+4].split()]


nbv = len(xnames)
varsparse = np.empty((dim,7,nbv))
covarsparse = np.empty((dim,7,7))
corsparse = np.empty((dim,7,7))
for i in range(dim):
    if int(boundary[i]) !=1:
        for j in range(nbv):
            for k in range(6):
                varsparse[i,k,j] = difData[j][int(neighbours[i][k])]
            varsparse[i,6,j] = difData[j][i]
        covarsparse[i,:,:] = np.cov(varsparse[i,:,:])
        corsparse[i,:,:] = np.corrcoef(varsparse[i,:,:])
print(covarsparse[88907,:,:])
print(corsparse[88907,:,:])


positions =[]
variances =[]
radcovar=[]
polcovar=[]
torcovar=[]
angles = list(set([r[1] for r in coords]))
torpos = angles[4]
print(torpos, 'torpos')
for i in range(len(coords)):
    if coords[i][1] == torpos and int(boundary[i]) !=1:
        positions.append(coords[i])
        variances.append(avVar[i])
        radcovar.append((corsparse[i,6,1]+corsparse[i,6,0])/2)
        polcovar.append((corsparse[i,6,3]+corsparse[i,6,2])/2)
        torcovar.append((corsparse[i,6,5]+corsparse[i,6,4])/2)

print(len(positions))
r = [x[0] for x in positions]
z = [x[2] for x in positions]

plot4 = plt.figure(13)
plt.xlabel('Radial Position')
plt.ylabel('Vertical Position')
sc1 = plt.scatter(r,z,c=variances, cmap='plasma', norm=matplotlib.colors.LogNorm())    
plt.colorbar(sc1)
plt.title('Variances' )

plot8 = plt.figure(8)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Variance'
plt.title(title)
sc1 = plt.scatter(coords[:,0], coords[:,2], s=20, c=avVar, cmap='plasma')
plt.colorbar(sc1)
plt.title('Variances')

plot6 = plt.figure(14)
plt.xlabel('Cell Volume')
plt.ylabel('Variance')
sc1 = plt.scatter(volumes,avVar)
plt.title('Variance vs Volume')





plot9 = plt.figure(9)
sc1 = plt.scatter(r,z,c=radcovar, cmap='plasma')  
plt.colorbar(sc1)
plt.xlabel('Horizontal Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Radial Covariance')

plot9 = plt.figure(10)
sc1 = plt.scatter(r,z,c=polcovar, cmap='plasma') 
plt.colorbar(sc1)
plt.title('Poloidal Covariance')
plt.xlabel('Horizontal Position [cm]')
plt.ylabel('Vertical Position [cm]')

plot9 = plt.figure(11)
sc1 = plt.scatter(r,z,c=torcovar, cmap='plasma') 
plt.colorbar(sc1)
plt.title('Toroidal Covariance')
plt.xlabel('Horizontal Position [cm]')
plt.ylabel('Vertical Position [cm]')



def monexp(x,L):
    return np.exp(-x/L)


#plot = plt.figure(8)
#plt.hist(covarsparse[:,6,0],bins=50)
#plot = plt.figure(9)
#plt.hist(covarsparse[:,6,1],bins=50)
#plot = plt.figure(10)
#plt.hist(covarsparse[:,6,2],bins=50)
#plot = plt.figure(11)
#plt.hist(covarsparse[:,6,3],bins=50)
#plot = plt.figure(12)
#plt.hist(covarsparse[:,6,4],bins=50)
#plot = plt.figure(13)
#plt.hist(covarsparse[:,6,5],bins=50)
plt.show()
