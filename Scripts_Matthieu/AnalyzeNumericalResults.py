import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
#import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions

# Constants
path = '/u/mbj/Drifts/data-for-drift-computations/ResolutionStudies/RadRes/Case50/OutputLSQDescr1/'
edrifts_file = open(path+'ExB_RESULTS','r')
driftpos_file = open(path+'DRIFT_POS','r')
efield_file = open(path+'EFIELD','r')
gradb_file = open(path+'GRADB', 'r')
gdrifts_file = open(path+'GRADB_DRIFTS', 'r')
edrifts = []
drift_pos = []
efield = []
gradb = []
gdrifts = []

# Read numeric drift values
for line in edrifts_file:
    edrifts.append(line)
    
for line in gdrifts_file:
    gdrifts.append(line)

# Read drift positions
for line in driftpos_file:
    drift_pos.append(line)

# Electric field
for line in efield_file:
    efield.append(line) 
    
# GradB
for line in gradb_file:
    gradb.append(line)

# Convert values from fortran to float, filter boundary cells and compute analytical profiles for cell center locations
[pos, enum, denum,gnum,dgnum, enumperp,denumperp, gnumperp ,dgnumperp,ea , ba, dea, ga,dga] = AnalysisFunctions.extractValues(drift_pos,edrifts,
 efield, gradb, gdrifts, AnalyticalPrescriptions.e_descr3,AnalyticalPrescriptions.b_field_description1,
 AnalyticalPrescriptions.gradb_description1, driftFunctions.ExBdrift, driftFunctions.gradBdrift)

# Compute vectors and magnitude + errors for efield and drift
[edrift_res_mag,edrift_an_mag,edrift_er_mag,edrift_er_vec,edrift_er_rel_mag,edrift_er_rel_vec] = AnalysisFunctions.extractVectorInfo(denum,dea)
[efield_res_mag,efield_an_mag,efield_er_mag,efield_er_vec,efield_er_rel_mag,efield_er_rel_vec] = AnalysisFunctions.extractVectorInfo(enum,ea)
[gdrift_res_mag,gdrift_an_mag,gdrift_er_mag,gdrift_er_vec,gdrift_er_rel_mag,gdrift_er_rel_vec] = AnalysisFunctions.extractVectorInfo(dgnum,dga)
[gradb_res_mag,gradb_an_mag,gradb_er_mag,gradb_er_vec,gradb_er_rel_mag,gradb_er_rel_vec] = AnalysisFunctions.extractVectorInfo(gnum,ga)


# Take slices in different directions to allow for extra analysis

torsurf = pos[0][1]
radsurf = pos[150][0] # Based on inspection

# Create a dictionary containing the different slices
torSliceDict = {}
keys = ['edrifts', 'efield','edrifts_perp','efield_perp' ,'edrift_mag', 'edrift_an_mag','edrift_er_mag', 'edrift_vec_er','edrift_er_rel_mag', 'edrift_er_rel_vec',
        'ef_mag', 'ef_an_mag', 'ef_mag_er', 'ef_vec_er','efield_er_rel_mag', 'efield_er_rel_vec', 
        'gdrifts', 'gradb','gdrifts_perp','gradb_perp','gdrift_mag','gdrift_an_mag','gdrift_er_mag', 'gdrift_vec_er' ,'gdrift_er_rel_mag', 'gb_er_rel_vec',
	'gb_mag','gb_an_mag', 'gb_mag_er', 'gb_vec_er','gb_er_rel_mag','gb_er_rel_vec','pos']

allInfo = [denum, enum,denumperp,enumperp, edrift_res_mag,edrift_an_mag,edrift_er_mag,edrift_er_vec,edrift_er_rel_mag,edrift_er_rel_vec,
           efield_res_mag,efield_an_mag,efield_er_mag,efield_er_vec,efield_er_rel_mag,efield_er_rel_vec,
           dgnum,gnum, dgnumperp, gnumperp, gdrift_res_mag,gdrift_an_mag,gdrift_er_mag,gdrift_er_vec,gdrift_er_rel_mag,gdrift_er_rel_vec,
           gradb_res_mag,gradb_an_mag,gradb_er_mag,gradb_er_vec,gradb_er_rel_mag,gradb_er_rel_vec]

torslices = AnalysisFunctions.getAllSlices(pos,allInfo,1,torsurf)
for i in range(len(torslices)):
    torSliceDict[keys[i]] = torslices[i]

#print(torSliceDict.get('ef_mag'))
#print(torSliceDict.get('ef_an_mag'))
#print(torSliceDict.get('efield_er_rel_mag'))
#print('max',max(torSliceDict.get('efield_er_rel_mag')))
#print(len(torSliceDict.get('efield_er_rel_mag')))
# Plot results

x = [r[0] for r in torSliceDict.get('pos')]
y = [r[1] for r in torSliceDict.get('pos')]
er = [x[0] for x in torSliceDict.get('efield')]
ez = [x[1] for x in torSliceDict.get('efield')]


plot1 = plt.figure(1)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of Electic Field'
plt.title(title)
sc1 = plt.scatter(x, y, s=20, c=torSliceDict.get('ef_mag'), cmap='plasma')
plt.colorbar(sc1)
plt.savefig(path+title+'.png')

plot2 = plt.figure(2)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Theta component of Electric Field'
plt.title(title)
sc2 = plt.scatter(x, y, s=20, c = [x[1] for x in torSliceDict.get('efield')], cmap='plasma')
plt.colorbar(sc2)
plt.savefig(path+title+'.png')

plot3 = plt.figure(3)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of Electric Field'
plt.title(title)
sc3 = plt.scatter(x, y, s=20, c=torSliceDict.get('efield_er_rel_mag'), cmap='plasma')
plt.colorbar(sc3)
plt.savefig(path+title+'.png')

plot4 = plt.figure(4)
plt.title('Potential')
PlottingFunctions.plot2DContour(Constants.ns,Constants.ns,Constants.Rmajor-Constants.Rminor,Constants.Rmajor+Constants.Rminor,-Constants.Rminor,Constants.Rminor,Constants.Rmajor,Constants.Rminor,AnalyticalPrescriptions.potential_description1,'X [cm]', 'Y [cm]','Potential [V]',False)
plt.savefig(path+title+'.png')

plot5 = plt.figure(5)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of ExB Drift (Numerical)'
plt.title(title)
sc5 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_mag'), cmap='plasma')
plt.colorbar(sc5)
plt.savefig(path+title+'.png')


plot6 = plt.figure(6)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of ExB Drift (Analytical)'
plt.title(title)
sc6 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_an_mag'), cmap='plasma')
plt.colorbar(sc6)
plt.savefig(path+title+'.png')


plot7 = plt.figure(7)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of ExB Drift'
plt.title(title)
sc7 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_er_rel_mag'), cmap='plasma')
plt.colorbar(sc7)
plt.savefig(path+title+'.png')
plot8 = plt.figure(8)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Magnitude of Efield_Rad')
sc8 = plt.scatter(x, y, s=20, c= [x[0] for x in torSliceDict.get('efield_perp')], cmap='plasma')
plt.colorbar(sc8)
plt.savefig(path+title+'.png')
plot9 = plt.figure(9)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Magnitude of Efield (perp)')
sc9 = plt.scatter(x, y, s=20, c= [x[2] for x in torSliceDict.get('efield_perp')], cmap='plasma')
plt.colorbar(sc9)
plt.savefig(path+title+'.png')

plt.show()

