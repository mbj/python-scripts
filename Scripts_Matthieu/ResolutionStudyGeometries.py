from MeshFunctions import *
import os
import shutil
import AnalyticalPrescriptions
import InputsGeometry
# The folder where the different cases should be stored is
basepath = '/u/mbj/Drifts/data-for-drift-computations'
folder = '/u/mbj/Drifts/data-for-drift-computations/ResolutionFinal'
os.mkdir(folder)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Analytical Profiles
#potentialdescription = AnalyticalPrescriptions.potential_description1
bfielddescription = AnalyticalPrescriptions.bfieldstrength1
# General Info
# toroidal range (degrees)
trange = 36
# Major and minor radius (considering a torus) (in cm!)
Rmajor = 500
Rminor = 100

# Loop over different cases to prepare
nts = [5, 10, 20, 30]
nrs = [10, 20, 40, 60]
nps = [40, 80, 160, 240]

#cases = [nrs,nps]
caseNamesT = ['Case1', 'Case2', 'Case3','Case4']
caseNamesP = ['Case1', 'Case2', 'Case3','Case4']
caseNamesR = ['Case1', 'Case2', 'Case3','Case4']
# Number of toroidal, radial, poloidal surfaces


# First toroidal resolution
nr = nrs[2]
np = nps[2]
path = folder + "/TorRes"
os.mkdir(path)
for i in range(len(nts)):
    # A specific folder for the case should be defined
    path = folder + "/TorRes/" + caseNamesT[i]
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    nt = nts[i]
    
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)


nr = nrs[2]
nt = nts[2]  
path = folder + "/PolRes"
os.mkdir(path)     
for i in range(len(nps)):
    # A specific folder for the case should be defined
    path = folder + "/PolRes/" + caseNamesP[i]
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    np = nps[i]
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)


nt = nts[2]
np = nps[2]   
path = folder + "/RadRes"
os.mkdir(path)
for i in range(len(nrs)):
    # A specific folder for the case should be defined
    path = folder + "/RadRes/" + caseNamesR[i]
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    nr = nrs[i]
    
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)
   
