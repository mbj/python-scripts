import numpy as np
import os
import matplotlib.pyplot as plt
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import PlottingFunctions
import AnalysisFunctions
import sys
from scipy import sparse
import scipy.optimize as optimize
import math
import matplotlib
import statistics
from scipy import stats
path = os.getcwd()
sys.path.append('../../emc3-grid-generator')
from gg import GG

xnames = ['_1','_2','_3','_4','_5','_6','_7','_8','_9']#,'_10']
Tdata = []

def monexp(x,L):
    return np.exp(-x/L)


for i in range(len(xnames)):
    file = open('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI','r')
    Temptemp = [r.split() for r in file.readlines()]
    Temp = []
    for i in Temptemp:
        Temp.extend(i)
    Tempel = Temp[0:int(len(Temp)/2)]
    Tempar = np.array(Tempel, dtype=float)
    Tdata.append(Tempar)


avTemp = Tdata[0]    
for i in range(1,len(xnames)):
    print(Tdata[i][50000:50010])
    avTemp = np.add(avTemp,Tdata[i])
avTemp = avTemp*1/len(xnames)

difData = []  
normdifData = []  
variance = []
for i in range(len(xnames)):
    difTemp = np.subtract(avTemp,Tdata[i])
#    normdifTemp = np.divide(difTemp,avTemp)
    var = np.square(difTemp)
    variance.append(var)
    difData.append(difTemp)
#    normdifData.append(normdifTemp) 
    
avDif = difData[0]    
avVar = variance[0]
for i in range(1,len(xnames)):
    avDif = np.add(avDif,difData[i])
    avVar = np.add(avVar,variance[i])
avDif = avDif*1/len(xnames)
avVar = avVar*1/len(xnames)
posdata = AnalysisFunctions.readText(path+'/Output1Descr6Noise0/DRIFT_POS')
# Load position data
geoData = AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/Output5Descr6Noise0/NBS')
volumes =np.array(AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/Output3Descr3Noise0/VOLUMES'),dtype=float)
plot=plt.figure(1)
########### Get covariance data => Try to fit correlation length model, looking at individual surfaces ##############################
# Get single error
limits = AnalysisFunctions.readText(path+'/Output1Descr6Noise0/LIMITS')
dim = math.floor(len(geoData)/5)
#TSS = [[15],[4],[15],[4],[15],[4],[15],[4],[15],[4],[15],[1]]
RSS=[[54],[66],[78],[95],[85],[92],[100],[113],[88],[72]]
PSS = [[100],[50],[150],[200],[360],[180],[210],[260],[315],[450]]
RSS=[[72]]
PSS=[[430]]
colors=["b","g","r","c","m","y","k","purple","grey","orange"]
choice=2
rs=[]
zs=[]
for k in range(len(RSS)):
    TS=[12]
    PS=PSS[k]
    RS=RSS[k]
    positions = []
    difvalues=[]
    variances =[]
    indices=[]
    vols=[]
    for i in range(dim):
        coords =[float(x) for x in geoData[5*i+2].split()]
        limT = [float(x) for x in limits[4*i+1].split()]
        limP = [float(x) for x in limits[4*i+2].split()]
        limR = [float(x) for x in limits[4*i+3].split()]
        if choice==1:
            if limT[0] in TS and limR[0] in RS:
               positions.append(coords)
               difvalues.append([r[i] for r in difData])
               indices.append(limP[0])
        elif choice == 2:
           if limT[0] in TS and limP[0] in PS:
               positions.append(coords)
               difvalues.append([r[i] for r in difData])
               indices.append(limR[0])
        else:
           if limR[0] in RS and limP[0] in PS:
               positions.append(coords)
               difvalues.append([r[i] for r in difData])    
               indices.append(limT[0])
    
    posArray = np.array(positions)
    difArray = np.array(difvalues)
    print(posArray[0])
    
    covmat = np.cov(difArray)
    cormat = np.corrcoef(difArray)
    cor1D = cormat[0]
    cov1D = covmat[0]

    distances = []
    poldistances = []
    polangles = []
    #indices=np.linspace(0,len(cor1D),len(cor1D))
#    for i in range(len(cor1D)):
#        dis = min(math.sqrt((positions[i][0]-positions[0][0])**2+(positions[i][2]-positions[0][2])**2+((positions[i][0]+positions[0][0])/2*abs(positions[i][1]-positions[0][1]))**2), \
#        math.sqrt((positions[i][0]-positions[35][0])**2+(positions[i][2]-positions[35][2])**2+((positions[i][0]+positions[35][0])/2*abs(positions[i][1]-positions[35][1]))**2))
#    poldis = abs(math.atan2(positions[i][2],positions[i][0]-580)-math.atan2(positions[0][2],positions[0][0]-580))*(math.sqrt((positions[i][0]-580)**2+(positions[i][2])**2)+math.sqrt((positions[0][0]-580)**2+(positions[0][2])**2))/2
#    polangle = math.atan2(positions[i][2],positions[i][0]-580)
#        distances.append(dis)




    xl = indices
    xlab='Toroidal Index '
    plot = plt.figure(1)
    sc1 = plt.scatter(xl,cor1D,color=colors[k])
    plt.xlabel(xlab)
    plt.ylabel('Correlation Ceofficient')
    plt.title('Correlation with Starting Cell')
    r = positions[0][0]
    z = positions[0][2]
    rs.append(r)
    zs.append(z)
    



print(rs,zs,'rz')
plot=plt.figure(2)
plt.title('Spatial Distribution of Considered Starting Points')

grid = GG()
dat = grid.read_mesh3d(fn='Inputs/GEOMETRY_3D_DATA')
grid.plot_mesh3d(dat,TS[0])
plt.xlabel('Horizontal Position')
plt.ylabel('Vertical Position')
print('done grid, now points please')
for i in range(len(PSS)):
    print('r',rs[i],'z',zs[i])
    plt.scatter(rs[i:i+1],zs[i:i+1],c=colors[i],s=50)
    #plt.plot(rs[i],zs[i],"r")#colors[i])
plt.plot(rs[0],zs[0],"r")
plt.show()
