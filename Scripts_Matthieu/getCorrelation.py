import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys
import numpy as np
from scipy import sparse


geoData = AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes/Output5Descr6Noise0/NBS')
dim = math.floor(len(geoData)/5)
print(dim)
boundary = np.empty(dim)
neighbours = np.empty((dim,6))
coords = np.empty((dim,3))

for i in range(dim):
    boundary[i] = geoData[5*i+1]
    neighbours[i][:] = [float(x) for x in geoData[5*i].split()]
    coords[i][:] =[float(x) for x in geoData[5*i+2].split()]

xnames = ['_1','_2','_3','_4','_5','_6','_7','_8','_9']#,'_10']
Tdata = []

for i in range(len(xnames)):
    file = open('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI','r')
    Temptemp = file.readlines()
    Temp = []
    for i in range(len(Temptemp)):
        Temp = Temp+[float(x) for x in Temptemp[i].split()]
#    print(Temp)
    print(len(Temp))
    Temp = np.array(Temp)
#    Temp = np.loadtxt('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI', max_rows = 25851)
#    Temp =np.concatenate(Temp.flatten(),np.loadtxt('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI',skiprows=25851,max_rows = 1))
#    Tlength = len(Temp)
#    print(Tlength)
#    Temp.flatten()
    Tdata.append(Temp)
    
avTemp = Tdata[0]    
for i in range(1,len(xnames)):
    avTemp = np.add(avTemp,Tdata[i])
avTemp = avTemp*1/len(xnames)

difData = []  
normdifData = []  
for i in range(len(xnames)):
    difTemp = np.absolute(np.subtract(avTemp,Tdata[i]))
    normdifTemp = np.divide(difTemp,avTemp)
    difData.append(difTemp)
    normdifData.append(normdifTemp)
    
avDif = difData[0]    
for i in range(1,len(xnames)):
    avDif = np.add(avDif,difData[i])
avDif = avDif*1/len(xnames)

print(len(avDif))
