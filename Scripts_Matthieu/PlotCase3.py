from mpl_toolkits import mplot3d
import numpy as nump
import matplotlib
import matplotlib.pyplot as plt
from MeshFunctions import *
import math
from PlottingFunctions import *
from TracingFieldLines import *
from plotSingleFluxTube import *
from driftFunctions import *
# General Info about the case to plot
nt =10; nr = 5; np = 20
Rmajor = 500
rminor = 100
Trange = 36
# Create the vertices
m2d = create_2D_mesh(nr,np,Rmajor,rminor,'toroidal')

# Create the analytical description of the B field
def bvectorcase3(r,theta,z, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)
    btheta = math.exp(-R*0.01)  # R in cm
    br = -z/R*1.5
    bz = (r-Rmajor)/R*1.5
    return [br,btheta,bz]
# Create the 3D mesh using the field lines
precision = 100
m3d = extend_mesh_3D(m2d,Trange,bvectorcase3,nt,precision, Rmajor, rminor)
rad = m3d[0]
hor = m3d[1]
tor = m3d[2]
field_lines = m3d[3]

# Get the field lines in x,y,z
lines = []
for i in range(len(field_lines)):
    field = []
    x = []
    y = []
    z = []
    for j in range(len(field_lines[i])):
        x.append(field_lines[i][j][0] * math.cos(field_lines[i][j][2] * math.pi / 180))
        y.append(field_lines[i][j][0] * math.sin(field_lines[i][j][2] * math.pi / 180))
        z.append(field_lines[i][j][1])
    field = [x,y,z]
    lines.append(field)


# Plot lines connecting the vertices in all different directions, this will be used to plot the mesh
[xlines_pol,xlines_rad,xlines_tor,ylines_pol,ylines_rad,ylines_tor,zlines_pol,zlines_rad,zlines_tor] = plotMesh(tor,rad,hor,nt,nr,np,lines,show=True)

# plot a single flux tube
plotfluxTube(tor,rad,hor,nt,nr,np,None,True)

#def b_field_description(r,z,theta, Rmajor, Rminor):
#    # Radial B-field (linear and max at central surfaces of the torus
#    R = math.sqrt((r-Rmajor)**2 + z**2)
#    R = 0.01*R
#    bval = math.exp(-R) # R in cm
#    return bval
#x,z,Bf = plot2DContour(100,100,Rmajor-rminor,Rmajor+rminor,-rminor,rminor,Rmajor,rminor,b_field_description,'','','',False)

#[xdata,ydata,zdata] = plotGridPoints(tor,rad,hor,show = False)

# Evaluate the electric field at all points:
def e_field_description(r,theta,z, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)
    # Potential as 1/r => Efield: -1/r^2
    er = -(r-Rmajor)/Rmajor/(((r-Rmajor)**2+z**2)/Rmajor)**(3/2)
    etheta = 0
    ez = -z/Rmajor/(((r-Rmajor)**2+z**2)/Rmajor)**(3/2)
    return [er, etheta, ez]

def gradB(r,theta,z,Rmajor,rminor):
    gbr = -(r-Rmajor)/(Rmajor*math.sqrt(z**2+(r-Rmajor)**2))*math.exp(-math.sqrt(z**2+(r-Rmajor)**2)/Rmajor)
    gbz = -z/(Rmajor*math.sqrt(z**2+(r-Rmajor)**2))*math.exp(-math.sqrt(z**2+(r-Rmajor)**2)/Rmajor)
    return [gbr, 0, gbz]



# Plot only the first outer surface and 2 toroidal lines
#plt.figure()
#ax = plt.figure().add_subplot(projection='3d')
#ax.plot(xlines_pol[nr-1], ylines_pol[nr-1], zlines_pol[nr-1],color='green')
#ax.plot(xlines_pol[(nt)*nr-1], ylines_pol[(nt)*nr-1], zlines_pol[(nt)*nr-1],color='green')
#ax.plot(xlines_tor[math.floor(np/4)*nr-1], ylines_tor[math.floor(np/4)*nr-1], zlines_tor[math.floor(np/4)*nr-1],color='green')
#ax.plot(xlines_tor[math.floor(np/2)*nr-1], ylines_tor[math.floor(np/2)*nr-1], zlines_tor[math.floor(np/2)*nr-1],color='green')
#ax.contourf(z,x,Bf,100,zdir='z')
#ax.set_xlabel('X [cm]')
#ax.set_ylabel('Y [cm]')
#ax.set_zlabel('Z [cm]')
#ax.set_title('Test Case 3')
#plt.show()

# Get two points, where the drifts (exb or gradb) will be computed and the vector plotted
p1 = [xlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)],ylines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)],zlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)]]
p2 = [xlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)],ylines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)],zlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)]]
p3= [xlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)+1],ylines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)+1],zlines_tor[math.floor(np/2)*nr-1][math.floor(nt/3)+1]]
p4 = [xlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)+1],ylines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)+1],zlines_tor[math.floor(np/4)*nr-1][math.floor(2*nt/3)+1]]
r1 = math.sqrt(p1[0]**2+p1[1]**2)
theta1 = math.atan2(p1[1],p1[0])
z1 = p1[2]
p1cyl = [r1,theta1,z1]
r2 = math.sqrt(p2[0]**2+p2[1]**2)
theta2 = math.atan2(p2[1],p2[0])
z2 = p2[2]
p2cyl = [r2,theta2,z2]

E1 = e_field_description(r1,theta1,z1,Rmajor,rminor)
B1 = bvectorcase3(r1,theta1,z1,Rmajor,rminor)
B2 = bvectorcase3(r2,theta2,z2,Rmajor,rminor)
gradB2 = gradB(r2,theta2,z2,Rmajor,rminor)
vexb = ExBdrift(E1,B1)
vgradb = gradBdrift(B2,gradB2)
# Plot only the first outer surface and 2 toroidal lines
fig = plt.figure()
ax = plt.axes(projection='3d')
scale = 25
E1 = cyltocart(E1,p1cyl)
print('E1', E1)
B1 = cylvec(B1,p1cyl)
B1 = nump.array(p3)-nump.array(p1)
print('B1', B1)
B2 = cylvec(B2,p2cyl)
B2 = nump.array(p4)-nump.array(p2)
print('B2', B2)
gradB2 = cyltocart(gradB2,p2cyl)
E1 = 500*scale*nump.array(E1)
B1 = 2*nump.array(B1)
B2 = 2*nump.array(B2)
gradB2 = 1000*scale*nump.array(gradB2)
vexb = 1000*scale*vexb
vgradb = 10*scale*vgradb

ax.quiver(p1[0],p1[1],p1[2],E1[0], E1[1],E1[2],color = 'yellow',label='Electric Field Vector')
ax.quiver(p1[0],p1[1],p1[2],B1[0], B1[1],B1[2],color = 'blue',label = 'Magnetic Field Vector')
ax.quiver(p1[0],p1[1],p1[2],vexb[0],vexb[1],vexb[2],color = 'red',label = 'E x B Drift Velocity')
ax.quiver(p2[0],p2[1],p2[2],gradB2[0], gradB2[1],gradB2[2],color = 'black',label = 'Magnetic Field Strength Gradient')
ax.quiver(p2[0],p2[1],p2[2],B2[0], B2[1],B2[2],color = 'blue')
ax.quiver(p2[0],p2[1],p2[2],vgradb[0],vgradb[1],vgradb[2],color = 'purple',label = 'Grad B Velocity')
ax.plot(xlines_pol[nr-1], ylines_pol[nr-1], zlines_pol[nr-1],color='green')
ax.plot(xlines_pol[(nt)*nr-1], ylines_pol[(nt)*nr-1], zlines_pol[(nt)*nr-1],color='green')
ax.plot(xlines_tor[math.floor(np/4)*nr-1], ylines_tor[math.floor(np/4)*nr-1], zlines_tor[math.floor(np/4)*nr-1],color='green')
ax.plot(xlines_tor[math.floor(np/2)*nr-1], ylines_tor[math.floor(np/2)*nr-1], zlines_tor[math.floor(np/2)*nr-1],color='green')
#ax.contourf(z,x,Bf,100,zdir='z')
ax.set_xlabel('X [cm]')
ax.set_ylabel('Y [cm]')
ax.set_zlabel('Z [cm]')
ax.set_title('Test Case 3')
ax.legend()
plt.show()