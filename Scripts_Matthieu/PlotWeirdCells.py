import numpy as np
import os
import matplotlib.pyplot as plt
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import PlottingFunctions
import AnalysisFunctions
import sys
from scipy import sparse
import math
from mpl_toolkits import mplot3d
path = os.getcwd()
sys.path.append('../../emc3-grid-generator')
from gg import GG
geoData = AnalysisFunctions.readText(path+'/Output2Descr1Noise0/DRIFT_POS')
limits = AnalysisFunctions.readText(path+'/Output2Descr1Noise0/LIMITS')
points = []
for i in range(len(geoData)):
  #  print(limits[4*i+1])
 #   print([x for x in limits[4*i+1].split()])
    lim = [float(x) for x in limits[4*i+1].split()]
#    lim = list(filter((' ').__ne__, lim))
#    lim = list(filter(('\n').__ne__, lim))
 #   print(lim)
#    lim = [float(x) for x in lim]
    
    if abs(lim[0]-lim[1])>1:
        pos = [float(x) for x in geoData[i].split()]
        points.append(pos)
        print(i)

x = [x[0]*math.cos(x[1]) for x in points]
z = [x[2] for x in points]
y = [x[0]*math.sin(x[1]) for x in points] 

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(x,y,z)
print(points)

angles = [r[1] for r in points]
uniqueangles = list(set(angles)) 
uniqueangles.sort()
print(uniqueangles)
print(len(uniqueangles))
count = []
for i in range(len(uniqueangles)):
    count.append(angles.count(uniqueangles[i]))
max_value = max(count)
max_index = count.index(max_value)
print('max',max_value,max_index)
torPos = uniqueangles[max_index]
ind = int(torPos/max(uniqueangles)*36-1)
rp = []
zp = []
for i in range(len(x)):
    if points[i][1] == torPos:
        rp.append(points[i][0])
        zp.append(points[i][2])    

print(torPos)

plot2 = plt.figure(2)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
sc2 = plt.scatter(rp,zp)
grid = GG()
dat = grid.read_mesh3d(fn='Inputs/GEOMETRY_3D_DATA')
grid.plot_mesh3d(dat,ind)


plt.show()


