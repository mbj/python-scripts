import matplotlib.pyplot as plt
import os
import sys
import AnalysisFunctions
import math
import seaborn as sns
import pandas as pd
plt.rcParams.update({'font.size': 12})
analyticalCase = sys.argv[1]
path = os.getcwd()
methods = sys.argv[3:]
print(methods)
noise = sys.argv[2]
methodnames = ['GG CC', 'LS CC', 'GG N', 'LS N', 'MAP']
methodsused = methods[0:len(methods)]
pathlengths = []
deviations = []
pathdeviations = []
reldev = []
relpdev = []
timesteps = []
errors = []
errorsmag = []
dataT = pd.DataFrame()
for k in range(len(methods)):
    method =methods[k]
    print(method)
    pls = []
    devs = []
    pdevs = []
    rds = []
    rpds = []
    ts = []
    errs = []
    erms = []
    dpls=[]
    aPL = 0
    trajInfo = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalCase+'Noise'+noise+'/TRAJECTORY_INFO')
    dim = len(trajInfo)
    for i in range(int(dim/4)):
        t = [float(x) for x in trajInfo[4*i].split()]
        er = [float(x) for x in trajInfo[4*i+1].split()]
        ersign=[float(x) for x in trajInfo[4*i+2].split()]
        anvel=[float(x) for x in trajInfo[4*i+3].split()]
        anpl = (math.sqrt(anvel[0]**2+(anvel[1]+1000)**2+anvel[2]**2))*100*0.1
        pls.append(t[0])
        dpls.append(abs(anpl-pls[-1])/anpl)
        devs.append(abs(t[1]))
        ts.append(t[2])
        rds.append(devs[-1]/pls[-1])
        errs.append(er)
        erm = math.sqrt(er[0]**2+er[1]**2+er[2]**2)
        erms.append(erm)
        onedata = {'Method':methodnames[k],'Tracing Error':erm, 'Deviation':abs(t[1]), 'Pathlength':t[0],'Relative Deviation':rds[-1],'Poloidal Velocity Error':er[2], 'Radial Velocity Error':er[0], 'PolVel':abs(ersign[2]),'RadVel':abs(ersign[0]), 'AnVel':anvel,'PDL':dpls[-1]}
        dataT = dataT.append(onedata,ignore_index=True)
#        pdevs.append(abs(t[0]-aPL))
#        rpds.append(pdevs[-1]/pls[-1])
    pathlengths.append(pls)
    deviations.append(devs)
#    pathdeviations.append(pdevs)
    reldev.append(rds)
#    relpdev.append(rpds)
    timesteps.append(ts)
    errors.append(errs)
    errorsmag.append(erms)

upperX = 1e-4
lowerX = 1e-9
upperY = 100
lowerY = 1e-12  
xlabel = 'Timestep [s]'
ylabel = 'Relative Deviation (after 1s)'


plot1 = plt.figure(1)
for i in range(len(methods)):
    sc1 = plt.scatter(timesteps[i],reldev[i],label = methodsused[i])

ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Relative Deviation '+analyticalCase+' [-] ')
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'Reldev.pdf')

#plot2 = plt.figure(2)
#sc1 = plt.scatter(timesteps[0],relpdev[0],label = methods[0])
#sc2 = plt.scatter(timesteps[1],relpdev[1],label = methods[1])
#sc3 = plt.scatter(timesteps[0],relpdev[2],label = methods[2])
#sc4 = plt.scatter(timesteps[1],relpdev[3],label = methods[3])
#sc5 = plt.scatter(timesteps[0],reldev[4],label = methods[4])

#ax = plt.gca()
#ax.set_yscale('log')
#ax.set_xscale('log')
#limits = [lowerX, upperX, lowerY, upperY*100]
#ax.legend()
##x = np.linspace(0.000001,1,100)
##y = x**(1)/100
##plt.loglog(x,y)
#plt.axis(limits)
#plt.title('Relative Pathlength Deviation'+analyticalCase)
#plt.xlabel(xlabel)
#plt.ylabel(ylabel)
#plt.savefig(path+'/Descr'+analyticalCase+'PathReldev.png')

plot3 = plt.figure(3)
for i in range(len(methods)):
    sc1 = plt.scatter(timesteps[i],errorsmag[i],label = methodsused[i])


ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Average Velocity Error '+analyticalCase +' [-] ')
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'AvVelErr.pdf')



#sns.violinplot(methods,errorsmag,hue=noiselevels)
#plt.show()
plot4=plt.figure(4)
plot = sns.violinplot(x=dataT['Method'],y='Tracing Error',data=dataT, scale="area", cut=0.1)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.title('Average Velocity Error [-]')
plt.savefig(path+'/Descr'+analyticalCase+'AvVelErr.pdf')


plot5=plt.figure(5)
plot = sns.violinplot(x=dataT['Method'],y='Relative Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Relative Deviation [-]')
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.savefig(path+'/Descr'+analyticalCase+'RelDevViolin.pdf')

plot6=plt.figure(6)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='Radial Velocity Error',data=dataT, scale="area", cut=0.1)
plt.title('Radial Velocity Error [-]')
plt.savefig(path+'/Descr'+analyticalCase+'RadVelViolin.pdf')

plot6=plt.figure(7)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='Poloidal Velocity Error',data=dataT, scale="area", cut=0.1)
plt.title('Poloidal Velocity Error [-]')
plt.savefig(path+'/Descr'+analyticalCase+'PolVelViolin.pdf')

plot6=plt.figure(8)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='PolVel',data=dataT, scale="area", cut=0.1)
plt.title('Poloidal Velocity Error (Bias) [-]')
plt.savefig(path+'/Descr'+analyticalCase+'PolVelBiasViolin.pdf')

plot6=plt.figure(9)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='RadVel',data=dataT, scale="area", cut=0.1)
plt.title('Radial Velocity Error (Bias) [-]')
plt.savefig(path+'/Descr'+analyticalCase+'RadVelBiasViolin.pdf')

plot6=plt.figure(10)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='PDL',data=dataT, scale="area", cut=0.1)
plt.title('Relative Pathlength Difference [-]')
plt.savefig(path+'/Descr'+analyticalCase+'Relative Pathlength Difference.pdf')

print(dataT)
plt.show()
	
