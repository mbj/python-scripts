import pandas as pd
import AnalysisFunctions
import seaborn as sns
import matplotlib.pyplot as plt
#df = AnalysisFunctions.readAllInfo('GG','1','Case20/')
#da = df.loc[:,'edrifts'].to_list()
#print(da[1])
#print(type(da))
#print(da)

import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys

# Analyze the performance and errors of different methods
# This script does this for given analytical prescriptions for potential and
# magnetic field
# Specify which analytical case is considered and which cases should be 
# calculated
analyticalCase = sys.argv[1]
Cases = sys.argv[2:]
path = os.getcwd()
print(path)
print(Cases)
method = 'LSQ' 

# Define structures to be computed    
key = 'efield_er_rel_mag'
avErrors = []
MaxErrors = []
nbs = []


errorsDF = pd.DataFrame()
errors = []
nbCells = []
maxErrors = []
for case in Cases:
    caseInfo = AnalysisFunctions.readAllInfo(method, analyticalCase, case +'/')
    caseErrors = caseInfo.get(key)
    tempDF = pd.DataFrame({case: caseErrors})
    errorsDF = pd.concat([errorsDF,tempDF], axis = 1)
    y = len(caseErrors)
    errors.append(sum([x/y for x in caseErrors]))
    nbCells.append(y)
    maxErrors.append(max(caseErrors))
    print(errors)
    print(maxErrors)
    print(nbCells)
plot = plt.figure()
sns.violinplot(data = errorsDF)
plt.savefig(path+'/Descr'+analyticalCase+method+'Violin.png')	
plt.show()
