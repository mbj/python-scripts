import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import sys
import os
import seaborn as sns

method = 'LSQ'
anCase = '1'
#Case = sys.argv[3]
path = '' # Case+'/'
allInfo = []

plot = plt.figure(1)
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [1e3, 1e7, 1e-7, 1]
nbs = [1e4, 1e5, 1e6]
MaxErrors = [1e-1, 1e-3, 1e-5]
plt.axis(limits)
sc1 = plt.scatter(nbs,MaxErrors,label = 'Max Error ')
vl = plt.vlines([2000, 5000, 10000],1e-7, 1,colors = ['g','r','b'])
plt.show()

