import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import PlottingFunctions
import AnalysisFunctions
import sys
import os
import seaborn as sns
import numpy as np
sys.path.append('../../emc3-grid-generator')
from gg import GG
method = sys.argv[1]
anCase = sys.argv[2]
noise = sys.argv[3]
#weight = sys.argv[4]
path = '' # Case+'/'
allInfo = []
allInfoGB = []

[info,infoGB] = AnalysisFunctions.readAllInfo(method,anCase,noise,'') #Case+'/'
keys = list(info.keys())
keysGB = list(infoGB.keys())

for i in range(len(keys)-1): # don't include pos
    allInfo.append(info.get(keys[i]))
for i in range(len(keysGB)-1): # don't include pos
    allInfoGB.append(infoGB.get(keysGB[i]))
pos = info.get('pos')
gbpos = infoGB.get('gb_pos')

angles = [r[1] for r in pos]
uniqueangles = list(set(angles))    
print(uniqueangles)
uniqueangles.sort()
# Get a slice for 2D visualization

ind=3
torsurf = [uniqueangles[ind]]
print(torsurf)
torSliceDict = {}
torslices = AnalysisFunctions.getAllSlices(pos,allInfo,1,torsurf)
for i in range(len(torslices)):
    torSliceDict[keys[i]] = torslices[i]
    

angles = [r[1] for r in gbpos]
uniqueanglesGB = list(set(angles))     
torsurfGB = [uniqueanglesGB[33]]
torSliceDictGB = {}
torslicesGB = AnalysisFunctions.getAllSlices(gbpos,allInfoGB,1,torsurfGB)
for i in range(len(torslicesGB)):
    torSliceDictGB[keysGB[i]] = torslicesGB[i]   
     
# Make Plots
x = [r[0] for r in torSliceDict.get('pos')]
y = [r[1] for r in torSliceDict.get('pos')]

xg = [r[0] for r in torSliceDictGB.get('gb_pos')]
yg = [r[1] for r in torSliceDictGB.get('gb_pos')]

# Transform Efield vectors to x,y
Ex = [r[0] for r in torSliceDict.get('efield_perp')]
Ey = [r[2] for r in torSliceDict.get('efield_perp')]

GBx = [r[0] for r in torSliceDictGB.get('gradb_perp')]
GBy = [r[2] for r in torSliceDictGB.get('gradb_perp')]

nb = 90
length = len(Ex)
step = int(length/nb-1)
plt.figure(1)
fig, ax = plt.subplots()
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Electric Field'
plt.title(title)
count=0
countl=0
#grid = GG()
#dat = grid.read_mesh3d(fn='Inputs/GEOMETRY_3D_DATA')
#grid.plot_mesh3d(dat,ind)
for i in range(0,length):
    if i%step == 0: #or (x[i]< 550 and y[i] < -90 and count <10) or (x[i]<540 and y[i]>90 and countl<10):
        plt.quiver(x[i],y[i],Ex[i], Ey[i])
        if x[i]<540 and y[i]<-90:
            count=count+1
            print(count, 'count')
        elif x[i]<540 and y[i]>90:
            countl=countl+1
            print(countl, 'countl')


# Transform Efield vectors to x,y
Edx = [r[0] for r in torSliceDict.get('edrifts_perp')]
Edy = [r[2] for r in torSliceDict.get('edrifts_perp')]

Gdx = [r[0] for r in torSliceDict.get('gdrifts_perp')]
Gdy = [r[2] for r in torSliceDict.get('gdrifts_perp')]

length = len(Ex)
step = int(length/nb-1)
plt.figure(2)
ax = plt.subplots()
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'ExB Drifts'
plt.title(title)
count=0
countl=0

for i in range(0,length):
    if i%step == 0:# or (x[i]< 550 and y[i] < -90 and count <10) or (x[i]<540 and y[i]>90 and countl<10):
        plt.quiver(x[i],y[i],Edx[i], Edy[i])
        if x[i]<540 and y[i]<-90:
            count=count+1
            print(count, 'count')
        elif x[i]<540 and y[i]>90:
            countl=countl+1
            print(countl, 'countl')
    

plt.savefig(method+anCase+noise+'VelocityFieldB.pdf')

length = len(Gdx)
step = int(length/nb-1)
plt.figure(3)
fig, ax = plt.subplots()
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Diamagnetic Drifts'
plt.title(title)
count=0
countl=0

for i in range(0,length):
    if i%step == 0:# or (x[i]< 550 and y[i] < -90 and count <10) or (x[i]<540 and y[i]>90 and countl<10):
        plt.quiver(x[i],y[i],Gdx[i], Gdy[i])
        if x[i]<540 and y[i]<-90:
            count=count+1
            print(count, 'count')
        elif x[i]<540 and y[i]>90:
            countl=countl+1
            print(countl, 'countl')
    


plt.savefig(method+anCase+noise+'GBVelocityFieldB.pdf')
plt.show()
