import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys

path = os.getcwd()
method = sys.argv[1]
analyticalP = sys.argv[2]
noise = sys.argv[3]
#weight = sys.argv[4]
aP = int(analyticalP)

positions = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/DRIFT_POS')
edrifts = AnalysisFunctions.readText(path+'/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/ExB_RESULTS')
efield = AnalysisFunctions.readText(path + '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/EFIELD')
gradb = AnalysisFunctions.readText(path +'/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/GRADB')
gdrifts = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/GRADB_DRIFTS')
boundarycells =  AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/BOUNDARYCELLS')
boundarynodes = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/BOUNDARYNODES')
gbpositions =  AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/GB_POS')
ggcorr = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/GGCORR')
potential =  AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/POTENTIAL_C')

Rmajor = 500
if aP ==1:
    edescr = AnalyticalPrescriptions.e_descr1
    pdescr = AnalyticalPrescriptions.potential_description1
elif aP == 5:
    edescr = AnalyticalPrescriptions.e_descr5
    pdescr = AnalyticalPrescriptions.potential_description5
elif aP==6:
    edescr = AnalyticalPrescriptions.e_descr6
    pdescr = AnalyticalPrescriptions.potential_description1
    Rmajor = 590
elif aP==7:
    edescr= AnalyticalPrescriptions.e_descr7
    pdescr = AnalyticalPrescriptions.potential_description2
elif aP >= 2:
    edescr = AnalyticalPrescriptions.e_descr2
    pdescr = AnalyticalPrescriptions.potential_description2
    
bdescr = AnalyticalPrescriptions.b_field_description1
gdescr = AnalyticalPrescriptions.gradb_description1
exb = driftFunctions.ExBdrift
gB = driftFunctions.gradBdrift

allInfo = AnalysisFunctions.getAllInfo(positions,gbpositions,edrifts,efield,gradb,gdrifts,boundarycells,boundarynodes,edescr,bdescr,gdescr,exb, gB, ggcorr,potential,pdescr,Rmajor, method+'Descr'+analyticalP+'Noise'+noise)

print('CSV file written')
