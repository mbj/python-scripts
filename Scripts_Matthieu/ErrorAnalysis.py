import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys
import numpy as np
import matplotlib
plt.rcParams.update({'font.size': 12})

# Analyze the performance and errors of different methods
# This script does this for given analytical prescriptions for potential and
# magnetic field
# Specify which analytical case is considered and which cases should be 
# calculated
analyticalCase = sys.argv[1]
n=int(sys.argv[2])
Study=sys.argv[3]
Cases = sys.argv[4:]
noise = '0'
path = os.getcwd()
print(path)
print(Cases)
methodnames = ['GG Cell-centered', 'LS Cell-centered', 'GG Nodal', 'LS Nodal','Mapping', 'GG exact surfaces'] 
methods = ['1','2','3','4','5', '6']
# Define structures to be computed    
efield_error_key = 'efield_er_rel_mag'
L2Errors = []
MeanErrors = []
SingleErrors = []
MaxErrors = []
GGErrors = []
lens = []

nbs = []
r = 530
phi = 0.15
z = 45
gg = 10
for method in methods:
#    errorsDF = pd.DataFrame()
    errorsL2 = []
    errorsMean = []
    errorsSingle = []
    nbCells = []
    charLength = []
    maxErrors = []
    errorsGG = []
    distances = []
    distancesnontor = []
    singledistances = []
    singledistancesnontor = []
    
    for case in Cases:
        siner = 1000
        dmin = 1000000000
        distmax = 0
        distnontormax = 0
        print(case)
        [caseInfo, caseInfoGB] = AnalysisFunctions.readAllInfo(method, analyticalCase, noise, case)
        caseErrors = caseInfo.get(efield_error_key)
        ggErrors = caseInfo.get('GGcorr')
        positions = caseInfo.get('pos')
#	errorsDF[case] = caseErrors
        y = len(caseErrors)
        errorsL2.append(math.sqrt(sum([x**2/y for x in caseErrors])))
        errorsMean.append(sum([x for x in caseErrors])/y)
        nbCells.append(y/n)
        vol = AnalysisFunctions.readText(path+'/'+case+'Output'+method+'Descr'+analyticalCase+'Noise'+noise+'/VOLAV')
        vol = float(vol[0])
#        print(vol)
        charLength.append((vol/10**6)**(1/3))
        maxErrors.append(max(caseErrors))
# Read geometrical data
        geoData = AnalysisFunctions.readText(path+'/'+case+'Output'+method+'Descr'+analyticalCase+'Noise'+noise+'/NBS')	
# Get single error
        for i in range(len(caseErrors)):
           ipos = positions[i]
           d = (z-ipos[2])**2+(r-ipos[0])**2+(phi-ipos[0])**2
           if d<dmin:
               siner = caseErrors[i] 
               gg = ggErrors[i]
               dmin = d
               singlepos = ipos
        for i in range(math.floor(len(geoData)/5)):
            ipos = [float(x) for x in geoData[5*i+2].split()]
            dis = [float(x) for x in geoData[5*i+3].split()] + [float(x) for x in geoData[5*i+4].split()]
            bound = int(geoData[5*i+1])
            dist = max(dis)
#            print(bound, dist,distmax)
#            print(bound == 0)
#            print(type(bound))
            distnontor = max(dis[0:4])
            if dist > distmax and bound == 0:
                distmax = dist
            if distnontor > distnontormax and bound == 0:
                distnontormax = distnontor
            if np.array_equal(ipos,singlepos):
                sindist = dist
                sindistnontor = distnontor
                
        errorsSingle.append(siner)
        errorsGG.append(gg)	
        distances.append(distmax)
        distancesnontor.append(distnontormax)
        singledistances.append(sindist)
        singledistancesnontor.append(sindistnontor)
#        print(errors)
#        print(maxErrors)
#        print(nbCells)
	
    L2Errors.append(errorsL2)
    MeanErrors.append(errorsMean)
    SingleErrors.append(errorsSingle)
    MaxErrors.append(maxErrors)
    GGErrors.append(errorsGG)
    nbs.append(nbCells)
    lens.append(charLength)
#    plot = plt.figure()
#    sns.violinplot(data = case[methods[0]])
#    plt.savefig(path+'/Descr'+analyticalCase+method+'Violin.png')
print('L2',L2Errors)
print('Mean', MeanErrors)
print('Single', SingleErrors)
print('Max',MaxErrors)
print('GG',GGErrors)
print(nbs) 
print('distances', distances)
print('distancesnontor', distancesnontor)
print(singledistances)
print(singledistancesnontor)
print('char length', lens)
# Compute measure for gradient lengths and typical grid size
typsize = 50*36*150
#maxGL = 
#avGL =  
 

upperX = 5*1e-1
lowerX = 5*1e-3
upperY = 1e-1
lowerY = 1e-6   

##########################################################
# Characteristic Length


xlabel = 'Characteristic Cell Length [m]'
ylabel = 'Relative Magnitude Error'
xarray = lens[0]


plot1 = plt.figure(1)
v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(xarray,MeanErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,MeanErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,MeanErrors[2],label = methodnames[2]) 
sc4 = plt.scatter(xarray,MeanErrors[3],label = methodnames[3]) 
sc5 = plt.scatter(xarray,MeanErrors[4],label = methodnames[4]) 
#sc6 = plt.scatter(xarray,MeanErrors[5],label = methodnames[5])
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Mean Errors Description '+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MeanError'+xlabel+'.pdf')

plot2 = plt.figure(2)
sc1 = plt.scatter(xarray,L2Errors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,L2Errors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,L2Errors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,L2Errors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,L2Errors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
plt.title('L2 Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'L2Error'+xlabel+'.pdf')


plot3 = plt.figure(3)
sc1 = plt.scatter(xarray,MaxErrors[0],label = 'Max Error '+methodnames[0])
sc2 = plt.scatter(xarray,MaxErrors[1],label = 'Max Error '+methodnames[1])
sc3 = plt.scatter(xarray,MaxErrors[2],label = 'Max Error '+methodnames[2])
sc4 = plt.scatter(xarray,MaxErrors[3],label = 'Max Error '+methodnames[3])
sc5 = plt.scatter(xarray,MaxErrors[4],label = 'Max Error '+methodnames[4])
#sc5 = plt.scatter(xarray,MaxErrors[5],label = 'Max Error '+methodnames[5])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Max Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MaxError'+xlabel+'.pdf')

plot4 = plt.figure(4)
sc1 = plt.scatter(xarray,GGErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,GGErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,GGErrors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,GGErrors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,GGErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
or1 = plt.loglog(x,y,label = 'first order')
or2 = plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('GG Correction Error Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'GG correction Error'+xlabel+'.pdf')


xlabel = 'Max Cell Length [m]'
ylabel = 'Relative Magnitude Error'
xarray = distances
upperX = 1e0
lowerX = 5*1e-2

######################################################################
# Max distance

plot5 = plt.figure(5)
v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(xarray,MeanErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,MeanErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,MeanErrors[2],label = methodnames[2]) 
sc4 = plt.scatter(xarray,MeanErrors[3],label = methodnames[3]) 
sc5 = plt.scatter(xarray,MeanErrors[4],label = methodnames[4]) 
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/400
y2 = x**2/400
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Mean Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MeanError'+xlabel+'.pdf')

plot6 = plt.figure(6)
sc1 = plt.scatter(xarray,L2Errors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,L2Errors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,L2Errors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,L2Errors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,L2Errors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('L2 Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'L2Error'+xlabel+'.pdf')


plot7 = plt.figure(7)
sc1 = plt.scatter(xarray,MaxErrors[0],label = 'Max Error '+methodnames[0])
sc2 = plt.scatter(xarray,MaxErrors[1],label = 'Max Error '+methodnames[1])
sc3 = plt.scatter(xarray,MaxErrors[2],label = 'Max Error '+methodnames[2])
sc4 = plt.scatter(xarray,MaxErrors[3],label = 'Max Error '+methodnames[3])
sc5 = plt.scatter(xarray,MaxErrors[4],label = 'Max Error '+methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/200
y2 = x**2/200
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
ax.legend()
plt.title('Max Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MaxError'+xlabel+'.pdf')

plot8 = plt.figure(8)
sc1 = plt.scatter(xarray,GGErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,GGErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,GGErrors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,GGErrors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,GGErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
ax.legend()
plt.title('GG Correction Error Description'+analyticalCase+xlabel)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'GG correction Error'+xlabel+'.pdf')

############################################################################
# Max length, non toroidal
xlabel = 'Max Length (excluding toroidal direction) [m]'
ylabel = 'Relative Magnitude Error'
xarray = distancesnontor

upperX = 5*1e-1
lowerX = 5*1e-3 

plot9 = plt.figure(9)
v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(xarray,MeanErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,MeanErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,MeanErrors[2],label = methodnames[2]) 
sc4 = plt.scatter(xarray,MeanErrors[3],label = methodnames[3]) 
sc5 = plt.scatter(xarray,MeanErrors[4],label = methodnames[4]) 
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
ax.legend()
plt.title('Mean Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MeanError'+xlabel+'.pdf')

plot10 = plt.figure(10)
sc1 = plt.scatter(xarray,L2Errors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,L2Errors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,L2Errors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,L2Errors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,L2Errors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
ax.legend()
plt.title('L2 Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'L2Error'+xlabel+'.pdf')


plot11 = plt.figure(11)
sc1 = plt.scatter(xarray,MaxErrors[0],label = 'Max Error '+methodnames[0])
sc2 = plt.scatter(xarray,MaxErrors[1],label = 'Max Error '+methodnames[1])
sc3 = plt.scatter(xarray,MaxErrors[2],label = 'Max Error '+methodnames[2])
sc4 = plt.scatter(xarray,MaxErrors[3],label = 'Max Error '+methodnames[3])
sc5 = plt.scatter(xarray,MaxErrors[4],label = 'Max Error '+methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.axis(limits)
ax.legend()
plt.title('Max Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MaxError'+xlabel+'.pdf')

plot12 = plt.figure(12)
sc1 = plt.scatter(xarray,GGErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,GGErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,GGErrors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,GGErrors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,GGErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/10
y2 = x**2/10
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('GG Correction Error Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'GG correction Error'+xlabel+'.pdf')

###########################################################################"
# Nb of cells
xlabel = Study +' Resolution (Nb Cells) [-]'
ylabel = 'Relative Magnitude Error'
xarray = nbs[0]
upperX = 1e2
lowerX = 1
upperY = 1e-1
lowerY = 1e-6 


plot13 = plt.figure(13)
v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(xarray,MeanErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,MeanErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,MeanErrors[2],label = methodnames[2]) 
sc4 = plt.scatter(xarray,MeanErrors[3],label = methodnames[3]) 
sc5 = plt.scatter(xarray,MeanErrors[4],label = methodnames[4]) 
sc6 = plt.scatter(xarray,MeanErrors[5],label = methodnames[5]) 
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
x = np.linspace(1,1000,1000)
y = 1/x*1/100
y2 = 1/x**2
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Mean Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MeanError'+xlabel+'.pdf')

plot14 = plt.figure(14)
sc1 = plt.scatter(xarray,L2Errors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,L2Errors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,L2Errors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,L2Errors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,L2Errors[4],label = methodnames[4])
sc = plt.scatter(xarray,L2Errors[5],label = methodnames[5])
v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
x = np.linspace(1,1000,1000)
y = 1/x*1/100
y2 = 1/x**2
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('L2 Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'L2Error'+xlabel+'.pdf')


plot15 = plt.figure(15)
sc1 = plt.scatter(xarray,MaxErrors[0],label = 'Max Error '+methodnames[0])
sc2 = plt.scatter(xarray,MaxErrors[1],label = 'Max Error '+methodnames[1])
sc3 = plt.scatter(xarray,MaxErrors[2],label = 'Max Error '+methodnames[2])
sc4 = plt.scatter(xarray,MaxErrors[3],label = 'Max Error '+methodnames[3])
sc5 = plt.scatter(xarray,MaxErrors[4],label = 'Max Error '+methodnames[4])
sc = plt.scatter(xarray,MaxErrors[5],label = 'Max Error '+methodnames[5])
v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
x = np.linspace(1,1000,1000)
y = 1/x*1/100
y2 = 1/x**2
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Max Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MaxError'+xlabel+'.pdf')

plot16 = plt.figure(16)
sc1 = plt.scatter(xarray,GGErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,GGErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,GGErrors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,GGErrors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,GGErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
x = np.linspace(1,1000,1000)
y = 1/x*1/100
y2 = 1/x**2
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('GG Correction Error Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'GG correction Error'+xlabel+'.pdf')



############################################################################
# Single Cell
upperX = 1e0
lowerX = 5*1e-2
upperY = 1e-1
lowerY = 1e-6 
xlabel = 'Max Cell length [m]'
ylabel = 'Relative Magnitude Error'

plot = plt.figure(17)
sc1 = plt.scatter(singledistances,SingleErrors[0],label = methodnames[0])
sc2 = plt.scatter(singledistances,SingleErrors[1],label = methodnames[1])
sc3 = plt.scatter(singledistances,SingleErrors[2],label = methodnames[2])
sc4 = plt.scatter(singledistances,SingleErrors[3],label = methodnames[3])
sc5 = plt.scatter(singledistances,SingleErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
ax.legend()
plt.axis(limits)
plt.title('Single Cell Error'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'SingleError.pdf')


upperX = 1e-1
lowerX = 5*1e-3
xlabel = 'Max Cell length (excluding toroidal direction) [m]'
plot = plt.figure(18)
sc1 = plt.scatter(singledistancesnontor,SingleErrors[0],label = methodnames[0])
sc2 = plt.scatter(singledistancesnontor,SingleErrors[1],label = methodnames[1])
sc3 = plt.scatter(singledistancesnontor,SingleErrors[2],label = methodnames[2])
sc4 = plt.scatter(singledistancesnontor,SingleErrors[3],label = methodnames[3])
sc5 = plt.scatter(singledistancesnontor,SingleErrors[4],label = methodnames[4])

v2 = plt.vlines(typsize,lowerY,upperY, color = 'g')
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(1)/100
y2 = x**2/100
plt.loglog(x,y, label='first order')
plt.loglog(x,y2, label='second order')
plt.axis(limits)
ax.legend()
plt.title('Single Cell Error Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'Single Cell Error.pdf')





plt.show()
