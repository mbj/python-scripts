import numpy as np
#import scipy as sp
import matplotlib.pyplot as plt
from MeshFunctions import *
from TracingFieldLines import *
import math


# Test the functions
# Create the 2D mesh to start with
m2d = create_2D_mesh(10,10,'toroidal')
# Create the 3D mesh using the field lines
m3d = extend_mesh_3D(m2d,36,Bfieldvector,10,100000)

# Plotting
xdata = []; ydata = []; zdata = []
rad = m3d[0]
hor = m3d[1]
tor = m3d[2]
field_lines = m3d[3]
# Plot grid points
for i in range(len(tor)):
    for j in range(len(rad[i])):
            xdata.append(rad[i,j]*math.cos(tor[i]*math.pi/180))
            ydata.append(rad[i,j]*math.sin(tor[i]*math.pi/180))
            zdata.append(hor[i,j])


ax = plt.axes(projection='3d')
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Dark2')
# Plot the field lines
# Plot the field lines
# Get the field lines in x,y,z
lines = []
for i in range(len(field_lines)):
    field = []
    x = []
    y = []
    z = []
    for j in range(len(field_lines[i])):
        x.append(field_lines[i][j][0] * math.cos(field_lines[i][j][2] * math.pi / 180))
        y.append(field_lines[i][j][0] * math.sin(field_lines[i][j][2] * math.pi / 180))
        z.append(field_lines[i][j][1])
    field = [x,y,z]
    lines.append(field)

# Plot the field lines
for i in range(len(field_lines)):
    ax.plot(lines[i][0],lines[i][1],lines[i][2])
plt.show()


# plot only 2d original mesh
xdata = []; ydata = []; zdata = []
rad = m2d[0]
hor = m2d[1]
field_lines = m3d[3]
# Plot grid points
for j in range(len(rad)):
        xdata.append(rad[j]*math.cos(0*math.pi/180))
        ydata.append(rad[j]*math.sin(0*math.pi/180))
        zdata.append(hor[j])

ax = plt.axes(projection='3d')
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Dark2')

#plt.show()