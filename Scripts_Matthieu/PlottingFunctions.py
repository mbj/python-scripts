# Contains functions for plotting
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as nump
# Plot the grid points
def plotGridPoints(tor,rad,hor,show = True):
    xdata = []
    ydata = []
    zdata = []
    cdata = []
    for i in range(len(tor)):
        for j in range(len(rad[i])):
            xdata.append(rad[i, j] * math.cos(tor[i] * math.pi / 180))
            ydata.append(rad[i, j] * math.sin(tor[i] * math.pi / 180))
            zdata.append(hor[i, j])
            cdata.append(1)

    # fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.scatter3D(xdata, ydata, zdata, cdata, cmap='plasma')
    if show:
        plt.show()
    return [xdata,ydata,zdata]

def plotMesh(tor,rad,hor,nt,nr,np,field_lines,show=True):
    plt.figure()
    ax = plt.axes(projection='3d')
    # Lines following toroidal direction

    xlines_tor = [None] * np * nr
    ylines_tor = [None] * np * nr
    zlines_tor = [None] * np * nr
    for i in range(len(rad[0])):
        xtemp = []
        ytemp = []
        ztemp = []
        for k in range(0,nt):
            xtemp.append(rad[k, i] * math.cos(tor[k] * math.pi / 180))
            ytemp.append(rad[k, i] * math.sin(tor[k] * math.pi / 180))
            ztemp.append(hor[k, i])
        xlines_tor[i] = xtemp
        ylines_tor[i] = ytemp
        zlines_tor[i] = ztemp

    if field_lines == None:
        for i in range(len(rad[0])):
            ax.plot(xlines_tor[i], ylines_tor[i], zlines_tor[i], color='green')
    else:
        for i in range(len(field_lines)):
            ax.plot(field_lines[i][0], field_lines[i][1], field_lines[i][2],color='green')

    # Poloidal lines

    xlines_pol = [None] * nt * nr
    ylines_pol = [None] * nt * nr
    zlines_pol = [None] * nt * nr
    for i in range(0,nt):
        for j in range(nr):
            xtemp = []
            ytemp = []
            ztemp = []
            for k in range(np):
                xtemp.append(rad[i, k * nr + j] * math.cos(tor[i] * math.pi / 180))
                ytemp.append(rad[i, k * nr + j] * math.sin(tor[i] * math.pi / 180))
                ztemp.append(hor[i, k * nr + j])
            xtemp.append(xtemp[0])
            ytemp.append(ytemp[0])
            ztemp.append(ztemp[0])
            xlines_pol[i * nr + j] = xtemp
            ylines_pol[i *nr + j] = ytemp
            zlines_pol[i* nr + j] = ztemp
            ax.plot(xlines_pol[i * nr + j], ylines_pol[i  * nr + j], zlines_pol[i  * nr + j],
                    color='green')

    # Radial lines

    xlines_rad = [None] * np * nt
    ylines_rad = [None] * np * nt
    zlines_rad = [None] * np * nt
    for i in range(0,nt):
        for j in range(np):
            xtemp = []
            ytemp = []
            ztemp = []
            for k in range(nr):
                xtemp.append(rad[i, j * nr + k] * math.cos(tor[i] * math.pi / 180))
                ytemp.append(rad[i, j * nr + k] * math.sin(tor[i] * math.pi / 180))
                ztemp.append(hor[i, j * nr + k])
            xtemp.append(xtemp[0])
            ytemp.append(ytemp[0])
            ztemp.append(ztemp[0])
            xlines_rad[i  * nr + j] = xtemp
            ylines_rad[i  * nr + j] = ytemp
            zlines_rad[i  * nr + j] = ztemp
            ax.plot(xlines_rad[i * nr + j], ylines_rad[i * nr + j], zlines_rad[i * nr + j],
                    color='green')

    ax.set_xlabel('X [cm]')
    ax.set_ylabel('Y [cm]')
    ax.set_zlabel('Z [cm]')
    ax.set_title('Mesh for Test Case EMC3')
    if show:
        plt.show()

    return [xlines_pol,xlines_rad,xlines_tor,ylines_pol,ylines_rad,ylines_tor,zlines_pol,zlines_rad,zlines_tor]


def plot2DContour(xnb,ynb,xmin,xmax,ymin,ymax,Rmajor, rminor, analy, xlabel,ylabel,title,show):
    Bfield = [0] * xnb
    xpoints = nump.linspace(xmin, xmax, xnb)
    zpoints = nump.linspace(ymin, ymax, ynb)

    for i in range(xnb):
        Bfield[i] = [0] * ynb
        for j in range(ynb):
            Bfield[i][j] = analy(xpoints[j], zpoints[i], 0, Rmajor, rminor)
    plt.contourf(xpoints, zpoints, Bfield, 1000, cmap='plasma')
    plt.colorbar()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    if show:
        plt.show()
    return [xpoints,zpoints,Bfield]