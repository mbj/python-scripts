# Read the results from the EMC3 computation of drifts and compare to analytical
import math
from driftFunctions import *
drifts_file = open('C:/Users/matth/github/EMC3/TestCase/DRIFTS','r')
driftpos_file = open('C:/Users/matth/github/EMC3/TestCase/DRIFT_POS','r')
efield_file = open('C:/Users/matth/github/EMC3/TestCase/EFIELD','r')
drifts = []
drift_pos = []
efield = []
# Read numeric drift values
for line in drifts_file:
    drifts.append(line)

# Read drift positions
for line in driftpos_file:
    drift_pos.append(line)

# Electric field
for line in efield_file:
    efield.append(line)

print('length',len(drifts))
print(drift_pos)
first_drift = drifts[250]
second_drift = drifts[150]
first_pos = drift_pos[250]
second_pos = drift_pos[150]
first_el = efield[250]
second_el = efield[150]

fd = [float(x) for x in first_drift.split()]
fp = [float(x) for x in first_pos.split()]
fe = [float(x) for x in first_el.split()]
sd = [float(x) for x in second_drift.split()]
sp = [float(x) for x in second_pos.split()]
se = [float(x) for x in second_el.split()]

print('fp', fp)
print('sp', sp)
print('fd',fd)
print('sd', sd)
print('fe', fe)
print('se', se)

# Compute drift obtained analytically for the positions corresponding to the numerical drifts
# Analytical prescription for the potential should correspond with the description used to evaluate potential at
# plasma cell centers => match fortran and python!!!
def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return [0,bval,0]
def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return [0,bval,0]
# Create the potential data
# At the moment we use the same grid for magnetic and plasma cells
def potential_description(r,z,theta,Rmajor,Rminor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r-Rmajor)**2+z**2)+0.001
    potval = math.exp(-D)*500
    return [potval]
def potential_description(r,z,theta,Rmajor,Rminor):
    R = (r-Rmajor)/100+0.001
    potval = math.exp(-R)*100
    return potval

def e_descr(r,z,theta, Rmajor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r - Rmajor) ** 2 + z ** 2)
    Er = -500*math.exp(-D)*1/D*(r-Rmajor)
    Ephi = 0
    Ez = -500*math.exp(-D)*1/D*z
    return [Er,Ephi, Ez]

def e_descr(r,z,theta, Rmajor):
    D = (r - Rmajor)/100+0.001
    Er = -100*math.exp(-D)
    Ephi = 0
    Ez = 0
    return [Er,Ephi, Ez]
fda = ExBdrift(e_descr(fp[0],fp[2],fp[1],500),b_field_description(fp[0],fp[2],fp[1],500,100))
print('fda',fda)

sda = ExBdrift(e_descr(sp[0],sp[2],sp[1],500),b_field_description(sp[0],sp[2],sp[1],500,100))
print('sda',sda)


# Quick checks
pos = [  553.255879500000,0.104719755119660,8.43490275000000 ]
E = e_descr(pos[0],pos[2],pos[1],500)
B = b_field_description(pos[0],pos[2],pos[1],500,100)
D = ExBdrift(E,B)
potcheck = potential_description(562.81309, 0.1047197551199660, 20.40921, 500,100)
print(potcheck)
print("ALL", E, B, D)