import matplotlib.pyplot as plt
import os
import sys
import AnalysisFunctions
sys.path.append('../../emc3-grid-generator')
from gg import GG
import seaborn as sns
import pandas as pd
import numpy as np

path = os.getcwd()
method = sys.argv[1]
analyticalP = sys.argv[2]
noise = sys.argv[3]
name = sys.argv[4]
aP = int(analyticalP)

plt.rcParams.update({'font.size': 12})

plot1 = plt.figure(1)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Trajectory'+ name
plt.title(title)
for j in range(3,5):
    posNum = []
    posLen = []
    posanNum = []
    posanLen = []
    positions = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/TRAJECTORY'+str(j))
    dim = len(positions)
    for i in range(int(dim/4)):
        p = [float(x) for x in positions[4*i].split()]
        posNum.append(p)
        l = float(positions[4*i+1].split()[0])
        posLen.append(l)
        pan = [float(x) for x in positions[4*i+2].split()]
        posanNum.append(pan)
        lan = float(positions[4*i+3].split()[0])
        posanLen.append(lan)
    posLen=np.array(posLen)*1/max(posLen)
    x = [r[0] for r in posNum]
    y = [r[2] for r in posNum]
    xan = [r[0] for r in posanNum]
    yan = [r[2] for r in posanNum]



    sc = plt.scatter(x, y, s=5, c =posLen, cmap='Reds', label='Numeric'+str(i))
    scan = plt.scatter(xan, yan,c=posanLen,cmap='Greys', s=5, label='Analytical'+str(i))
#    plt.colorbar(sc)
#    plt.colorbar(scan)
    plt.xlabel('Radial Position [cm]')
    plt.ylabel('Vertical Position [cm]')
    title = 'Drift Orbits'
    plt.title(title)

grid = GG()
dat = grid.read_mesh3d(fn='Inputs/GEOMETRY_3D_DATA')
grid.plot_mesh3d(dat,3)
plt.savefig('Trajectories'+method+analyticalP+noise+'.pdf')
plt.show()

