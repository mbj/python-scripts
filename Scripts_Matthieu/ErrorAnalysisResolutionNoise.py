import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys
import numpy as np
import matplotlib
plt.rcParams.update({'font.size': 12})
# Analyze the performance and errors of different methods
# This script does this for given analytical prescriptions for potential and
# magnetic field
# Specify which analytical case is considered and which cases should be 
# calculated
analyticalCase = sys.argv[1]
Cases = sys.argv[2:]
noise = '23'
path = os.getcwd()
print(path)
print(Cases)
methodnames = ['GG Cell-centered', 'LS Cell-centered', 'GG Nodal', 'LS Nodal','Mapping']#, 'GG exact surfaces'] 
methods = ['1','2','3','4','5']#, '6']
# Define structures to be computed    
efield_error_key = 'efield_er_rel_mag'
L2Errors = []
MeanErrors = []
lens = []

nbs = []
r = 530
phi = 0.15
z = 45
gg = 10
for method in methods:
#    errorsDF = pd.DataFrame()
    errorsL2 = []
    errorsMean = []
    charLength = []

    
    for case in Cases:
        print(case)
        [caseInfo, caseInfoGB] = AnalysisFunctions.readAllInfo(method, analyticalCase, noise, case)
        caseErrors = caseInfo.get(efield_error_key)
#	errorsDF[case] = caseErrors
        y = len(caseErrors)
        errorsL2.append(math.sqrt(sum([x**2/y for x in caseErrors])))
        errorsMean.append(sum([x for x in caseErrors])/y)
        vol = AnalysisFunctions.readText(path+'/'+case+'Output'+method+'Descr'+analyticalCase+'Noise'+noise+'/VOLAV')
        vol = float(vol[0])
#        print(vol)
        charLength.append((vol/10**6)**(1/3))

    L2Errors.append(errorsL2)
    MeanErrors.append(errorsMean)
    lens.append(charLength)

print('L2',L2Errors)
print('Mean', MeanErrors)
print('char length', lens)

upperX = 5*1e-1
lowerX = 5*1e-3
upperY = 1
lowerY = 1e-2   

##########################################################
# Characteristic Length


xlabel = 'Characteristic Cell Length [m]'
ylabel = 'Relative Magnitude Error'
xarray = lens[0]


plot1 = plt.figure(1)
sc1 = plt.scatter(xarray,MeanErrors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,MeanErrors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,MeanErrors[2],label = methodnames[2]) 
sc4 = plt.scatter(xarray,MeanErrors[3],label = methodnames[3]) 
sc5 = plt.scatter(xarray,MeanErrors[4],label = methodnames[4]) 
#sc6 = plt.scatter(xarray,MeanErrors[5],label = methodnames[5])
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]

x = np.linspace(0.000001,1,100)
y = x**(-1)/100
y2 = x**(-2)/100
y12 = x**(-1/2)/100*3
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.loglog(x,y12, label = 'half order')
ax.legend()
plt.axis(limits)
plt.title('Mean Errors Description '+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'MeanError'+xlabel+'.pdf')

plot2 = plt.figure(2)
sc1 = plt.scatter(xarray,L2Errors[0],label = methodnames[0])
sc2 = plt.scatter(xarray,L2Errors[1],label = methodnames[1])
sc3 = plt.scatter(xarray,L2Errors[2],label = methodnames[2])
sc4 = plt.scatter(xarray,L2Errors[3],label = methodnames[3])
sc5 = plt.scatter(xarray,L2Errors[4],label = methodnames[4])

ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
x = np.linspace(0.000001,1,100)
y = x**(-1)/1000
y2 = x**(-2)/100
y12 = x**(-1/2)/100*3
plt.loglog(x,y,label = 'first order')
plt.loglog(x,y2, label = 'second order')
plt.loglog(x,y12, label = 'half order')
plt.axis(limits)
plt.title('L2 Errors Description'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'L2Error'+xlabel+'.pdf')


plt.show()
