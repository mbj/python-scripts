# This file defines all the quantities and creates the files required to run a test case for the computation of
# drift velocities in the EMC3 simulation code
from MeshFunctions import *
import os
import shutil
import AnalyticalPrescriptions
# The folder where the different cases should be stored is
folder = '/u/mbj/Drifts/data-for-drift-computations'
# A specific folder for the case should be defined
casefolder = "TestNoise"
# create the folder
path = folder+"/"+casefolder
# Check if the folder should be removed => Control to not loose simulations
if os.path.isdir(path):
    Flag = int(input("Do you want to remove"+path+" Answer should be 1/0"))
    if Flag:
        shutil.rmtree(path)
    else:
        path = folder+"\\"+input("Give a new folder name")
os.mkdir(path)
path = path+"/Inputs"
os.mkdir(path)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Number of toroidal, radial, poloidal surfaces
nt = 36
nr = 30
np = 150
# toroidal range (degrees)
trange = 36
# Major and minor radius (considering a torus) (in cm!)
Rmajor = 500
Rminor = 100
# Create the file with the general info
gi_path = path+"/input.geo"
CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])

bfielddescription = AnalyticalPrescriptions.bfieldstrength1
# Create the GEOMETRY_3D_DATA file
[tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
print(tor)
geopath = path+'/GEOMETRY_3D_DATA'
writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,geopath)

# Create the magnetic field data
# Create an analytical description
bfieldpath = path+"/BFIELD_STRENGTH"
imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)


