import matplotlib.pyplot as plt
import os
import sys
import AnalysisFunctions
import math
import seaborn as sns
import pandas as pd

analyticalCase = sys.argv[1]
path = os.getcwd()
methods = sys.argv[3:]
print(methods)
noise = sys.argv[2]
methodnames = ['GG CC', 'LS CC', 'GG N', 'LS N', 'MAP']
methodsused = methods[0:len(methods)]
pathlengths = []
deviations = []
pathdeviations = []
reldev = []
relpdev = []
timesteps = []
timestepss=[]
errors = []
errorsmag = []
dataT = pd.DataFrame()
for k in range(len(methods)):
    method =methods[k]
    print(method)
    pls = []
    devs = []
    pdevs = []
    rds = []
    rpds = []
    ts = []
    tss=[]
    errs = []
    erms = []
    aPL = 0
    trajInfo = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalCase+'Noise'+noise+'/TRAJECTORY_INFO')
    dim = len(trajInfo)
    for i in range(int(dim/2)):
        t = [float(x) for x in trajInfo[2*i].split()]
        er = [float(x) for x in trajInfo[2*i+1].split()]
        pls.append(t[0])
        devs.append(abs(t[1]))
        ts.append(round(math.log(t[2],10)))
        tss.append(t[2])
        rds.append(devs[-1]/pls[-1])
        errs.append(er)
        erm = math.sqrt(er[0]**2+er[1]**2+er[2]**2)
        erms.append(erm)
        onedata = {'Method':methodnames[k],'Tracing Error':erm, 'Deviation':abs(t[1]), 'Pathlength':t[0],'Relative Deviation':rds[-1], 'Time Step':t[2]}
        dataT = dataT.append(onedata,ignore_index=True)
#        pdevs.append(abs(t[0]-aPL))
#        rpds.append(pdevs[-1]/pls[-1])
    pathlengths.append(pls)
    deviations.append(devs)
#    pathdeviations.append(pdevs)
    reldev.append(rds)
#    relpdev.append(rpds)
    timesteps.append(ts)
    timestepss.append(tss)
    errors.append(errs)
    errorsmag.append(erms)

upperX = 1e-5
lowerX = 1e-10
upperY = 1e-4
lowerY = 1e-8 
xlabel = 'Timestep [s]'
ylabel = 'Relative Deviation (after 1s)'

print(reldev)

plot1 = plt.figure(1)
for i in range(len(methods)):
    print(reldev[i])
    print(timestepss[i])
    sc1 = plt.scatter(timestepss[i],reldev[i],label = methodnames[i])

ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Relative Deviation'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'ReldevTS.pdf')

upperY = 1e-2
lowerY = 1e-5
plot3 = plt.figure(3)
for i in range(len(methods)):
    sc1 = plt.scatter(timestepss[i],errorsmag[i],label = methodsused[i])


ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Average Velocity Error'+analyticalCase)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.savefig(path+'/Descr'+analyticalCase+'AvVelErrTS.pdf')
plt.show()


#sns.violinplot(methods,errorsmag,hue=noiselevels)
#plt.show()
plot4=plt.figure(4)
plot = sns.violinplot(x=dataT['Time Step'],y='Tracing Error',hue='Method',data=dataT, scale="area", cut=0.1)
plt.title('Average Velocity Error')
plt.savefig(path+'/Descr'+analyticalCase+'AvVelErrViolinTS.pdf')


plot5=plt.figure(5)
plot = sns.violinplot(x='Time Step',y='Relative Deviation',hue='Method',data=dataT, scale="area", cut=0.1)
plt.title('Relative Deviation')
plt.savefig(path+'/Descr'+analyticalCase+'RelDevViolinTS.pdf')

print(dataT)
plt.show()
	
