import matplotlib.pyplot as plt
import os
import sys
import AnalysisFunctions
import math
import numpy as np
import plotly.graph_objects as go

path = os.getcwd()
method = sys.argv[1]
analyticalP = sys.argv[2]
noise = sys.argv[3]
name = sys.argv[4]
aP = int(analyticalP)
posNum = []
posLen = []

#plot1 = plt.figure(1)
figure,axes = plt.subplots(1)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Trajectory'+ name
plt.title(title)
positions = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalP+'Noise'+noise+'/TRAJECTORY4')
for i in range(len(positions)):
    if i%2 == 0:
        p = [float(x) for x in positions[i].split()]
        posNum.append(p)
    else:
        l = float(positions[i])
        posLen.append(l)

x = [r[0] for r in posNum]
y = [r[2] for r in posNum]

R = math.sqrt((x[0]-500)**2+y[0]**2)
print(R)

sc = plt.scatter(x, y, s=50, c = posLen, cmap = 'plasma')
#circle = plt.Circle((500,0),R, color = 'green')
angle = np.linspace(0,2*np.pi,150)
x = (500+R*np.cos(angle))
y = R*np.sin(angle)

axes.plot(x,y)

plt.savefig(path+'/Trajectory'+method+analyticalP+noise+name+'.png')
print(path+method+analyticalP+noise+name+'.png')


x = [r[0]*math.cos(r[1]) for r in posNum]
y = [r[0]*math.sin(r[1]) for r in posNum]
z = [r[2] for r in posNum]

x = x[0:250]
y = y[0:250]
z = z[0:250]

#fig = plt.figure()
#ax = fig.add_subplot(projection='3d')
#sc = plt.scatter(x, y, z, cmap = 'plasma')


fig = go.Figure(data=[go.Scatter3d(x=x, y=y, z=z,
                                   mode='markers',
                                   marker=dict(
        size=12,
        color=posLen[0:250],                # set color to an array/list of desired values
        colorscale='Viridis',   # choose a colorscale
        opacity=0.8
    ))])
fig.show()
plt.show()
