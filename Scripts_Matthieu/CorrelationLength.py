import numpy as np
import os
import matplotlib.pyplot as plt
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import PlottingFunctions
import AnalysisFunctions
import sys
from scipy import sparse
import scipy.optimize as optimize
import math
import matplotlib
import statistics
from scipy import stats
path = os.getcwd()


xnames = ['_1','_2','_3','_4','_5','_6','_7','_8']#,'_9']#,'_10']
Tdata = []

def monexp(x,L):
    return np.exp(-x/L)


for i in range(len(xnames)):
    file = open('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/X-TEST-HR/x-out'+xnames[i]+'/x-out/TE_TI','r')
    Temptemp = [r.split() for r in file.readlines()]
    Temp = []
    for i in Temptemp:
        Temp.extend(i)
    Tempel = Temp[0:int(len(Temp)/2)]
    Tempar = np.array(Tempel, dtype=float)
    Tdata.append(Tempar)


avTemp = Tdata[0]    
for i in range(1,len(xnames)):
    print(Tdata[i][50000:50010])
    avTemp = np.add(avTemp,Tdata[i])
avTemp = avTemp*1/len(xnames)

difData = []  

for i in range(len(xnames)):
    difTemp = np.subtract(avTemp,Tdata[i])
    difData.append(difTemp)
    
avDif = difData[0]    

for i in range(1,len(xnames)):
    avDif = np.add(avDif,difData[i])

avDif = avDif*1/len(xnames)
# Load position data
posdata = AnalysisFunctions.readText(path+'/Reference-FullRes2/Output1Descr6Noise0/DRIFT_POS')
geoData = AnalysisFunctions.readText('/u/mbj/Drifts/data-for-drift-computations/Reference-FullRes2/Output5Descr6Noise0/NBS')
limits = AnalysisFunctions.readText(path+'/Reference-FullRes2/Output1Descr6Noise0/LIMITS')
dim = math.floor(len(geoData)/5)



polstart=150
polend=200
tend=15
tstart =10
npol=polend-polstart
nt=tend-tstart
corlengths = np.empty((npol,nt))
pvalues=np.empty((npol,nt))
for k in range(npol):
    for j in range(nt):
        positions = []
        difvalues=[]

        for i in range(dim):
            coords =[float(x) for x in geoData[5*i+2].split()]
            limT = [float(x) for x in limits[4*i+1].split()]
            limP = [float(x) for x in limits[4*i+2].split()]

            if limT[0]==j and limP[0] == k:
                positions.append(coords)
                difvalues.append([r[i] for r in difData])
    
        if len(positions) >0:
            posArray = np.array(positions)
            difArray = np.array(difvalues)
            covmat = np.cov(difArray)
            cormat = np.corrcoef(difArray)
            cor1D = cormat[0]
            cov1D = covmat[0]
            distances = []

            for i in range(len(cor1D)):
                dis = math.sqrt((positions[i][0]-positions[0][0])**2+(positions[i][2]-positions[0][2])**2+((positions[i][0]+positions[0][0])/2*abs(positions[i][1]-positions[0][1]))**2)
                distances.append(dis)


            xl = distances

            corlen=-1
            x = np.array(xl[0:corlen])
            y=np.array(cor1D[0:corlen])

#        cv,p=stats.pearsonr(x,y)
            popt,pcov=optimize.curve_fit(monexp,x,y)
            print('popt',popt,'pcov',pcov)
            corlengths[k,j] = popt
            pvalues[k,j] = pcov
        else:
            corlengths[k,j] = 0
            pvalues[k,j] = 2
	
corlen1D = np.reshape(corlengths,(nt*npol,-1))
pval1D = np.reshape(pvalues,(nt*npol,-1))

x=[]
y=[]
for i in range(len(corlen1D)):
    if pval1D[i][0] <2:
        x.append(corlen1D[i][0])
        y.append(pval1D[i][0])
print(x)
print(y)	
corlen1D = np.array(x)
pval1D = np.array(y)
print(corlen1D)
print(pval1D)	
plot = plt.figure(1)
sc = plt.scatter(corlen1D,pval1D)
plt.ylim((0,1))
plt.savefig(path+'CorrelationLengthsRadial.png')

plot2 = plt.figure(2)
# Creating plot
plt.hist2d(np.array(corlen1D),np.array(pval1D), bins=50,norm=matplotlib.colors.LogNorm())
plt.colorbar()
plt.savefig("RadialCorHistogram")
plt.show()

