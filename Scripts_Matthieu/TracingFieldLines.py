
import numpy as np
#import scipy as sp
import matplotlib.pyplot as plt
from MeshFunctions import *
import math


def create_2D_mesh(nb_rad, nb_pol,Rmajor,rminor,geometry):
    if geometry == 'toroidal':
        [toroidal_surfaces, radial_positions, vertical_positions] = create_toroidal_vertices(1,nb_rad, nb_pol, 0, Rmajor, rminor)

        return [radial_positions,vertical_positions]
    else:
        print('not implemented yet')
        return None


def numerical_fieldline(B,res,nb_tor,Trange,r0,Rmajor,rminor):
    points = []
    points.append(r0)
    r = []; z = []; theta = []
    i = 0
    while True:
        r = points[i][0]
        z = points[i] [1]
        theta = points[i][2]
        Bvector = B(r,theta,z,Rmajor,rminor)
        Bnorm = np.linalg.norm(Bvector)
        if Bnorm <0.001:
            print('BNORM small')
        n1 = points[i][0]+Bvector[0]*res/Bnorm
        n2 = points[i][1] + Bvector[2] * res / Bnorm
        n3 = points[i][2] + Bvector[1] * res / Bnorm
        points.append([n1,n2,n3])
        i = i+1
        if theta>=Trange:
            break
    return points

def extend_mesh_3D(mesh_2D,tor_range,B,nb_tor,precision, Rmajor, rminor):
    field_lines = []
    nb_2d = len(mesh_2D[0][0])
    tor_res = tor_range/nb_tor
    res = tor_res/precision
    tor_surfaces = np.linspace(0, tor_range, nb_tor)  # Include first and last surface
    # Initialize arrays for the radial and horizontal positions
    # Radial position R = R0 (major radius) + a (minor radius)*cos(phi)
    radial_positions = np.empty(shape=(nb_tor+1,nb_2d),dtype='float')
    horizontal_positions = np.empty(shape=(nb_tor+1,nb_2d),dtype='float')
    for i in range(nb_2d):
        r0 = [mesh_2D[0][0][i],mesh_2D[1][0][i],0]
        radial_positions[0,i] = r0[0]
        horizontal_positions[0,i] = r0[1]
        points = numerical_fieldline(B,res,nb_tor,tor_range,r0, Rmajor, rminor)
        next_tor = tor_res
        p = 1

        for j in range(len(points)):
            if points[j][2]>= next_tor:
                radial_positions[p,i] = points[j][0]
                horizontal_positions[p,i] = points[j][1]
                p+=1
                next_tor+=tor_res
        field_lines.append(points)
    return [radial_positions, horizontal_positions, tor_surfaces,field_lines]