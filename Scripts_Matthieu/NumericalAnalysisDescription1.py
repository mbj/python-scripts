import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
#import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions

# Constants
path = '/u/mbj/Drifts/data-for-drift-computations/Description1_bis/'
method = 'GG'

LSQfiles = []
LSQfiles.append('Case10')
LSQfiles.append('Case20')
LSQfiles.append('Case40')
LSQfiles.append('Case80')
LSQfiles.append('Case160')



errorsLSQ = []
nbCellsLSQ = []
errorsGG = []
nbCellsGG = []

for i in range(len(LSQfiles)):
    torSliceDict = AnalysisFunctions.getAllInfo(AnalysisFunctions.readText(path + LSQfiles[i] + '/Output' + method+'/DRIFT_POS'),
                                                AnalysisFunctions.readText(path+LSQfiles[i]+'/Output' + method+'/ExB_RESULTS'),
                                                AnalysisFunctions.readText(path + LSQfiles[i] + '/Output' + method+'/EFIELD'),
                                                AnalysisFunctions.readText(path + LSQfiles[i] + '/Output' + method+'/GRADB'),
                                                AnalysisFunctions.readText(path + LSQfiles[i] + '/Output' + method+'/GRADB_DRIFTS'),
                                                AnalyticalPrescriptions.e_descr2, AnalyticalPrescriptions.b_field_description1,
                                                AnalyticalPrescriptions.gradb_description1, 
                                                driftFunctions.ExBdrift, driftFunctions.gradBdrift)
    relerrors = torSliceDict.get('efield_er_rel_mag')
    y = len(relerrors)
    errorsLSQ.append(sum([x/y for x in relerrors]))
    nbCellsLSQ.append(len(relerrors))
    print(i)
    print(sum(relerrors))
    print(len(relerrors))
    print(sum(relerrors)/len(relerrors))

print(errorsLSQ)
print(nbCellsLSQ)
# For both methods, plot the error as a function of the number of cells used
#plot1 = plt.figure(1)
#sc1 = plt.scatter(nbCellsGG, errorsGG)
#ax = plt.gca()
#ax.set_yscale('log')
#ax.set_xscale('log')
plot2 = plt.figure(2)
sc2 = plt.scatter(nbCellsLSQ,errorsLSQ)
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')

limits = [1e4, 1e7, 1e-6, 1]
plt.axis(limits)
plt.savefig(path+method+'Error.png')
plt.show()
