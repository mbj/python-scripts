from MeshFunctions import *
import os
import shutil
import AnalyticalPrescriptions
# The folder where the different cases should be stored is
basepath = '/u/mbj/Drifts/data-for-drift-computations'
folder = '/u/mbj/Drifts/data-for-drift-computations/TestUnCorrelatedNoise'
os.mkdir(folder)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Loop over different cases to prepare
nts = [4,8,16,24,36]#, 12,18]#, 48]
nrs = [8,16,32,48,64]#,80]
nps = [40,80,160,240,320]#,400]
#cases = [nrs,nps]
caseNames = ['Case1','Case2','Case3', 'Case4','Case5']#, 'Case4']#, 'Case5']
# Number of toroidal, radial, poloidal surfaces
bfielddescription = AnalyticalPrescriptions.bfieldstrength1
for i in range(len(nps)):
    # A specific folder for the case should be defined
    path = folder + "/" + caseNames[i]
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    nt = nts[i]
    nr = nrs[i]
    np = nps[i]
    # toroidal range (degrees)
    trange = 36
    # Major and minor radius (considering a torus) (in cm!)
    Rmajor = 500
    Rminor = 100
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)

  
