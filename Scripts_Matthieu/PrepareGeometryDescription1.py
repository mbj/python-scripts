from MeshFunctions import *
import os
import shutil
import AnalyticalPrescriptions
# The folder where the different cases should be stored is
basepath = '/u/mbj/Drifts/data-for-drift-computations'
folder = '/u/mbj/Drifts/data-for-drift-computations/StudyLSQ'
if os.path.isdir(folder):
    Flag = int(input("Do you want to remove" + folder + " Answer should be 1/0"))
    if Flag:
        shutil.rmtree(folder)
    else:
        folder = basepath + "/" + input("Give a new folder name")
os.mkdir(folder)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Loop over different cases to prepare
nts = [9, 18, 36, 48, 72]
nrs = [12, 25, 50, 75, 100]
nps = [38, 75, 150, 225, 300]
#cases = [nrs,nps]
caseNames = ['Case1', 'Case2', 'Case3','Case4', 'Case5']
# Number of toroidal, radial, poloidal surfaces

for i in range(len(nts)):
    # A specific folder for the case should be defined
    path = folder + "/" + caseNames[i]
    if os.path.isdir(path):
        Flag = int(input("Do you want to remove" + path + " Answer should be 1/0"))
        if Flag:
            shutil.rmtree(path)
        else:
            path = folder + "/" + input("Give a new folder name")
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    nt = nts[i]
    nr = nrs[i]
    np = nps[i]
    potentialdescription = AnalyticalPrescriptions.potential_description1
    bfielddescription = AnalyticalPrescriptions.bfieldstrength1
    # toroidal range (degrees)
    trange = 36
    # Major and minor radius (considering a torus) (in cm!)
    Rmajor = 500
    Rminor = 100
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)

    # Create the potential data
    # At the moment we use the same grid for magnetic and plasma cells

    potentialpath = path+"/POTENTIAL"
    imposeAnalytic(tor,rad,hor,potentialdescription,Rmajor, Rminor,potentialpath)
