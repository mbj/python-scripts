import numpy as nump
#import scipy as sp
import matplotlib.pyplot as plt
from MeshFunctions import *
from TracingFieldLines import *
from PlottingFunctions import *
from plotSingleFluxTube import *
import math
# Create the 2D mesh to start with
nt =6; nr = 2; np = 30
Rmajor = 500
rminor = 100
Trange = 36
m2d = create_2D_mesh(nr,np,Rmajor,rminor,'toroidal')
# Create the analytical description of the B field
def B(r,z,theta,Rmajor,rminor):
    R = math.sqrt((r - Rmajor) ** 2 + z ** 2) + 0.01
    R = 0.01*R
    # Find poloidal direction => helical field
    alpha = math.atan2(z,r-Rmajor)
    Bphimag = 3
    Br = Bphimag*-math.sin(alpha)
    Bz = Bphimag*math.cos(alpha)
    Btheta = 1 #math.exp(-R)
    return [Br,Bz,Btheta]
# Create the 3D mesh using the field lines
precision = 100
m3d = extend_mesh_3D(m2d,Trange,B,nt,precision, Rmajor, rminor)

rad = m3d[0]
hor = m3d[1]
tor = m3d[2]
field_lines = m3d[3]




# Get the field lines in x,y,z
lines = []
for i in range(len(field_lines)):
    field = []
    x = []
    y = []
    z = []
    for j in range(len(field_lines[i])):
        x.append(field_lines[i][j][0] * math.cos(field_lines[i][j][2] * math.pi / 180))
        y.append(field_lines[i][j][0] * math.sin(field_lines[i][j][2] * math.pi / 180))
        z.append(field_lines[i][j][1])
    field = [x,y,z]
    lines.append(field)


# Plot lines connecting the vertices in all different directions, this will be used to plot the mesh
plt.figure(2)
plotMesh(tor,rad,hor,nt,nr,np,lines,show=True)

# Plot the field lines
plt.figure()
ax = plt.axes(projection='3d')
for i in range(len(field_lines)):
    ax.plot(lines[i][0],lines[i][1],lines[i][2])
plt.show()



# plot a single flux tube
plotfluxTube(tor,rad,hor,nt,nr,np,None,True)
