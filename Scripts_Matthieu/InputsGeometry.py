import AnalyticalPrescriptions
from MeshFunctions import *
import shutil
import os
def createInputFiles(nz, nr,np,nt,path, trange, Rmajor, Rminor, bfield, potential):
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfield,Rmajor, Rminor, bfieldpath)

    # Create the potential data
    # At the moment we use the same grid for magnetic and plasma cells

    potentialpath = path+"/POTENTIAL"
    imposeAnalytic(tor,rad,hor,potential,Rmajor, Rminor,potentialpath)
