from MeshFunctions import *
import os
import shutil
import AnalyticalPrescriptions
# The folder where the different cases should be stored is
basepath = '/u/mbj/Drifts/data-for-drift-computations'
folder = '/u/mbj/Drifts/data-for-drift-computations/FinalNoNoise/'
os.mkdir(folder)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Loop over different cases to prepare
nts = [5, 9, 18, 36, 72]
nrs = [8, 15, 30, 60, 120]
nps = [50, 100, 200, 400, 800]
#cases = [nrs,nps]
caseNames = ['Case5_8_50','Case9_15_100','Case18_30_200','Case36_60_400','Case72_120_800']
# Number of toroidal, radial, poloidal surfaces
bfielddescription = AnalyticalPrescriptions.bfieldstrength1
for i in range(len(nps)):
    # A specific folder for the case should be defined
    path = folder + "/" + caseNames[i]
    os.mkdir(path)
    path = path + "/Inputs"
    os.mkdir(path)
    nt = nts[i]
    nr = nrs[i]
    np = nps[i]
    # toroidal range (degrees)
    trange = 36
    # Major and minor radius (considering a torus) (in cm!)
    Rmajor = 500
    Rminor = 100
    # Create the file with the general info
    gi_path = path+"/input.geo"
    CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


    # Create the GEOMETRY_3D_DATA file
    [tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
    print(tor)
    filepath = path+'/GEOMETRY_3D_DATA'
    writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,filepath)

    # Create the magnetic field data
    # Create an analytical description
    bfieldpath = path+"/BFIELD_STRENGTH"
    imposeAnalytic(tor,rad,hor,bfielddescription,Rmajor, Rminor, bfieldpath)

  
