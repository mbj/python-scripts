import matplotlib.pyplot as plt
import os
import sys
import AnalysisFunctions
import math
import seaborn as sns
import pandas as pd
plt.rcParams.update({'font.size': 12})
analyticalCase = sys.argv[1]
path = os.getcwd()
mn = sys.argv[2:]
lenmn=len(mn)
lenm=int(lenmn/2)
methods=mn[0:lenm]
print(methods)
noises=mn[lenm:]
noiseLevels=[0.3,0.6,0.9,1.2,1.5]
methodnames = ['GG CC', 'LS CC', 'GG N', 'LS N', 'MAP']
methodsused = methodnames[0:len(methods)]
dataT = pd.DataFrame()

for k in range(len(methods)):
    method =methods[k]
    print(method)
    for j in range(len(noises)):
        noise = noiseLevels[j]
        trajInfo = AnalysisFunctions.readText(path+ '/Output' + method+'Descr'+analyticalCase+'Noise'+noises[j]+'/TRAJECTORY_INFO')
        dim = len(trajInfo)
        for i in range(int(dim/4)):
            t = [float(x) for x in trajInfo[4*i].split()]
            er = [float(x) for x in trajInfo[4*i+1].split()]
            ersign=[float(x) for x in trajInfo[4*i+2].split()]
            anvel=[float(x) for x in trajInfo[4*i+3].split()]
            anpl = (math.sqrt(anvel[0]**2+(anvel[1]+1000)**2+anvel[2]**2))*100*0.1
            erm = math.sqrt(er[0]**2+er[1]**2+er[2]**2)
            onedata = {'Method':methodnames[k],'Tracing Error':erm, 'Deviation':abs(t[1]), 'Pathlength':t[0],'Relative Deviation':abs(t[1])/t[0],'Poloidal Velocity Error':er[2], 'Radial Velocity Error':er[0], 'PolVel':abs(ersign[2]),'RadVel':abs(ersign[0]), 'AnVel':anvel,'PDL':abs(anpl-t[0])/anpl, 'Noise Standard Deviation':noise}
            dataT = dataT.append(onedata,ignore_index=True)


plot1=plt.figure(1)
plot = sns.violinplot(x=dataT['Method'],y='Tracing Error',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.title('Average Velocity Error')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'AvVelErr.pdf')


plot2=plt.figure(2)
plot = sns.violinplot(x=dataT['Method'],y='Relative Deviation',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.title('Relative Deviation')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'RelDevViolin.pdf')

plot6=plt.figure(6)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='Radial Velocity Error',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Radial Velocity Error [-]')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'RadVelViolin.pdf')

plot6=plt.figure(7)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='Poloidal Velocity Error',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Poloidal Velocity Error [-]')
plt.savefig(path+'/Descr'+analyticalCase+'PolVelViolin.pdf')

plot6=plt.figure(8)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='PolVel',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Poloidal Velocity Error (Bias) [-]')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'PolVelBiasViolin.pdf')

plot6=plt.figure(9)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='RadVel',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Radial Velocity Error (Bias) [-]')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'RadVelBiasViolin.pdf')

plot6=plt.figure(10)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plot = sns.violinplot(x=dataT['Method'],y='PDL',hue='Noise Standard Deviation',data=dataT, scale="area", cut=0.1)
plt.title('Relative Pathlength Difference [-]')
plt.legend(loc=2, prop={'size': 10})
plt.savefig(path+'/Descr'+analyticalCase+'Relative Pathlength Difference.pdf')


plt.show()
	
