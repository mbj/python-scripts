import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions
import os
import sys
plt.rcParams.update({'font.size': 12})

# Analyze the performance and errors of different methods
# This script does this for given analytical prescriptions for potential and
# magnetic field
# Specify which analytical case is considered and which cases should be 
# calculated
analyticalCase = sys.argv[1]
#Cases = sys.argv[2:]
path = os.getcwd()
print(path)
#print(Cases)
case = ''
methods = ['11', '12', '13', '14', '15'] 
methodnames=['GG Cell-centered', 'LS Cell-centered','GG Nodal', 'LS Nodal', 'Mapping']
noiseLevels = [0,0.3,0.6,0.9,1.2,1.5]
noiseNames = ['0','21','22', '23', '24', '25']

# Define structures to be computed    
efield_error_key = 'efield_er_rel_mag'
exb_error_key = 'edrift_er_rel_mag'
avErrors = []
avErrorsD = []
MaxErrors = []
nbs = []

for method in methods:
    errors = []
    errorsD = []
    nbCells = []
    maxErrors = []
    for noise in noiseNames:
        print(case)
        [caseInfo,caseInfoGB] = AnalysisFunctions.readAllInfo(method, analyticalCase,noise, case)
        caseErrors = caseInfo.get(efield_error_key)
        caseErrorsD = caseInfo.get(exb_error_key)
        y = len(caseErrors)
        errors.append(sum([x/y for x in caseErrors]))
        errorsD.append(sum([x/y for x in caseErrorsD]))
        nbCells.append(y)
        maxErrors.append(max(caseErrors))
	
    avErrors.append(errors)
    avErrorsD.append(errorsD)
    MaxErrors.append(maxErrors)
    nbs.append(nbCells)
    
print(avErrors)
print(nbs) 
# Compute measure for gradient lengths and typical grid size
typsize = 50*36*150
#maxGL = 
#avGL =  
 

upperX = 2
lowerX = -0.01   
upperY = 10
lowerY = 1e-5   
plot1 = plt.figure(1)
#v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(noiseLevels,avErrors[0],color = 'red',label = 'Average Error '+methodnames[0])
sc2 = plt.scatter(noiseLevels,avErrors[1],color = 'green',label = 'Average Error '+methodnames[1])
sc3 = plt.scatter(noiseLevels,avErrors[2],color = 'blue',label = 'Average Error '+methodnames[2]) 
sc4 = plt.scatter(noiseLevels,avErrors[3],color = 'yellow',label = 'Average Error '+methodnames[3]) 
sc5 = plt.scatter(noiseLevels,avErrors[4],color = 'black',label = 'Average Error '+methodnames[4]) 
ax = plt.gca()
ax.set_yscale('log')
#ax.set_xscale('log')
limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Average Errors Description'+analyticalCase)
plt.xlabel('Noise SD Measure')
plt.ylabel('Relative Error')
plt.savefig(path+'/Descr'+analyticalCase+'Error.pdf')


upperX = 2
lowerX = -0.01   
upperY = 1
lowerY = 1e-2  
plot1 = plt.figure(2)
#v1 = plt.vlines(typsize,lowerY,upperY, color = 'g')
sc1 = plt.scatter(noiseLevels,avErrors[0],color = 'red',label = 'Average Error '+methodnames[0])
sc2 = plt.scatter(noiseLevels,avErrors[1],color = 'green',label = 'Average Error '+methodnames[1])
sc3 = plt.scatter(noiseLevels,avErrors[2],color = 'blue',label = 'Average Error '+methodnames[2]) 
sc4 = plt.scatter(noiseLevels,avErrors[3],color = 'yellow',label = 'Average Error '+methodnames[3]) 
sc5 = plt.scatter(noiseLevels,avErrors[4],color = 'black',label = 'Average Error '+methodnames[4]) 
ax = plt.gca()

limits = [lowerX, upperX, lowerY, upperY]
ax.legend()
plt.axis(limits)
plt.title('Average Errors Description'+analyticalCase)
plt.xlabel('Noise SD Measure')
plt.ylabel('Relative Error')
plt.savefig(path+'/Descr'+analyticalCase+'NoiseError.pdf')

plt.show() 
