# General small functions
import math
def listdif(l1,l2):
    l = []
    for i in range(len(l1)):
        l.append(l1[i]-l2[i])
    return l
def listnorm(l):
    norm = math.sqrt(sum([x**2 for x in l]))
    return norm