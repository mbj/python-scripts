import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import PlottingFunctions
import AnalysisFunctions
import sys
import os
import seaborn as sns
import numpy as np
from collections import Counter

plt.rcParams.update({'font.size': 12})
method = sys.argv[1]
anCase = sys.argv[2]
noise = sys.argv[3]
#weight = sys.argv[4]
path = '' # Case+'/'
allInfo = []
allInfoGB = []

[info,infoGB] = AnalysisFunctions.readAllInfo(method,anCase,noise,'') #Case+'/'
keys = list(info.keys())
keysGB = list(infoGB.keys())

for i in range(len(keys)-1): # don't include pos
    allInfo.append(info.get(keys[i]))
for i in range(len(keysGB)-1): # don't include pos
    allInfoGB.append(infoGB.get(keysGB[i]))
pos = info.get('pos')
gbpos = infoGB.get('gb_pos')

angles = [r[1] for r in pos]
uniqueangles = list(set(angles)) 
a = sorted(uniqueangles) 
b = Counter(a)
print('angles',a)
# Get a slice for 2D visualization
print('nb angles',len(uniqueangles))
torsurf = [a[3]]
print('chosen surface',torsurf)
torSliceDict = {}
torslices = AnalysisFunctions.getAllSlices(pos,allInfo,1,torsurf)
for i in range(len(torslices)):
    torSliceDict[keys[i]] = torslices[i]
    

angles = [r[1] for r in gbpos]
uniqueanglesGB = list(set(angles)) 
a = sorted(uniqueanglesGB)     
torsurfGB = [a[2]]
torSliceDictGB = {}
torslicesGB = AnalysisFunctions.getAllSlices(gbpos,allInfoGB,1,torsurfGB)
for i in range(len(torslicesGB)):
    torSliceDictGB[keysGB[i]] = torslicesGB[i]   
     
# Make Plots
x = [r[0] for r in torSliceDict.get('pos')]
y = [r[1] for r in torSliceDict.get('pos')]

potentials = [AnalyticalPrescriptions.potential_description1(r[0],r[2],r[1],500,100) for r in pos]
gLengths = AnalysisFunctions.gradientLength(torSliceDict.get('ef_an_mag'),potentials)

plot1 = plt.figure(1)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of Electric Field [V/m] (Numerical)'
cm=torSliceDict.get('ef_mag')
for i in range(len(cm)):
    if cm[i]>12000:
        cm[i]=math.nan
plt.title(title)
sc1 = plt.scatter(x, y, s=20, c=cm, cmap='plasma')
plt.colorbar(sc1)
plt.savefig(path+method+anCase+noise+'Magnitude of Electric Field (Numerical) .pdf')

plot2 = plt.figure(2)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Horizontal component of Electric Field'
plt.title(title)
sc2 = plt.scatter(x, y, s=20, c = [x[0] for x in torSliceDict.get('ef_an')], cmap='plasma')
plt.colorbar(sc2)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot3 = plt.figure(3)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of Electric Field'
cm=torSliceDict.get('efield_er_rel_mag')
for i in range(len(cm)):
    if cm[i]>1:
        cm[i]=math.nan
	
plt.title(title)
sc3 = plt.scatter(x, y, s=20, c=cm, cmap='plasma', norm=matplotlib.colors.LogNorm())
plt.colorbar(sc3)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot4 = plt.figure(4)
title = 'Potential [V]'
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title(title)
sc5 = plt.scatter(x, y, s=20, c = torSliceDict.get('potential'), cmap='plasma')
plt.colorbar(sc5)
plt.savefig(path+method+anCase+noise+title+'.pdf')


plot5 = plt.figure(5)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of Electric Field (Analytical)'
plt.title(title)
sc5 = plt.scatter(x, y, s=20, c = torSliceDict.get('ef_an_mag'), cmap='plasma')
plt.colorbar(sc5)
plt.savefig(path+method+anCase+noise+title+'.pdf')


plot6 = plt.figure(6)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of ExB Drift (Numerical)'
plt.title(title)
sc6 = plt.scatter(x, y, s=20, c=torSliceDict.get('edrift_mag'), cmap='plasma')
plt.colorbar(sc6)
plt.savefig(path+method+anCase+noise+title+'.pdf')


plot7 = plt.figure(7)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of ExB Drift'
plt.title(title)
sc7 = plt.scatter(x, y, s=20, c=torSliceDict.get('edrift_er_rel_mag'), cmap='plasma', norm=matplotlib.colors.LogNorm())
plt.colorbar(sc7)
plt.savefig(path+method+anCase+noise+title+'.pdf')


plot8 = plt.figure(8)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of Efield [V/m] (Radial)'
plt.title(title)
sc8 = plt.scatter(x, y, s=20, c= [x[0] for x in torSliceDict.get('efield')], cmap='plasma')
plt.colorbar(sc8)
plt.savefig(path+method+anCase+noise+'Magnitude of Efield (Radial).pdf')


plot9 = plt.figure(9)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of Efield [V/m] (Poloidal)'
plt.title(title)
sc9 = plt.scatter(x, y, s=20, c= [x[2] for x in torSliceDict.get('efield')], cmap='plasma')
plt.colorbar(sc9)
plt.savefig(path+method+anCase+noise+'Magnitude of Efield  (Poloidal).pdf')

plot10 = plt.figure(10)
sns.violinplot(torSliceDict.get('efield_er_rel_mag'))
title = 'ErrorDistribution'+method+anCase
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot11 = plt.figure(11)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'GradientLengths'
plt.title(title)
sc11 = plt.scatter(x, y, s=20, c=gLengths, cmap='plasma')
plt.colorbar(sc11)
plt.savefig(path+method+anCase+noise+title+'.pdf')

normErrors = np.multiply(np.array(torSliceDict.get('edrift_er_rel_mag')),np.array(gLengths))
plot12 = plt.figure(12)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'NormalizedRelErrors ExB'
plt.title(title)
cm=normErrors
for i in range(len(cm)):
    if cm[i]>1:
        cm[i]=math.nan
	
sc12 = plt.scatter(x, y, s=20,c=cm , cmap='plasma', norm=matplotlib.colors.LogNorm())
plt.colorbar(sc12)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot13 = plt.figure(13)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of GradB Drift'
plt.title(title)
sc13 = plt.scatter(x, y, s=20, c=torSliceDict.get('gdrift_er_rel_mag'), cmap='plasma')
plt.colorbar(sc13)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot14 = plt.figure(14)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of GradB Drift (Analytical)'
plt.title(title)
sc14 = plt.scatter(x, y, s=20, c=torSliceDict.get('gdrift_an_mag'), cmap='plasma')
plt.colorbar(sc14)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot15 = plt.figure(15)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of GradB Drift (Numerical)'
plt.title(title)
sc15 = plt.scatter(x, y, s=20, c=torSliceDict.get('gdrift_mag'), cmap='plasma')
plt.colorbar(sc15)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot16 = plt.figure(16)
sns.violinplot(torSliceDict.get('edrift_er_rel_mag'))
title = 'ErrorDistributionEDRIFT'+method+anCase
plt.savefig(path+method+anCase+noise+title+'.pdf')


plot20 = plt.figure(20)
title = 'Potential Noise'
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title(title)
sc20 = plt.scatter(x, y, s=20, c = torSliceDict.get('potdif'), cmap='plasma')
plt.colorbar(sc20)
plt.savefig(path+method+anCase+noise+title+'.pdf')

# ! Different position for gradB
x = [r[0] for r in torSliceDictGB.get('gb_pos')]
y = [r[1] for r in torSliceDictGB.get('gb_pos')]


plot17 = plt.figure(17)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of GradB (Analytical)'
plt.title(title)
sc17 = plt.scatter(x, y, s=20, c=torSliceDictGB.get('gb_an_mag'), cmap='plasma')
plt.colorbar(sc17)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot18 = plt.figure(18)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Magnitude of GradB (Numerical)'
plt.title(title)
sc18 = plt.scatter(x, y, s=20, c=torSliceDictGB.get('gb_mag'), cmap='plasma')
plt.colorbar(sc18)
plt.savefig(path+method+anCase+noise+title+'.pdf')

plot19 = plt.figure(19)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
title = 'Relative magnitude error of GradB '
plt.title(title)
sc19 = plt.scatter(x, y, s=20, c=torSliceDictGB.get('gb_er_rel_mag'), cmap='plasma', norm=matplotlib.colors.LogNorm())
plt.colorbar(sc19)
plt.savefig(path+method+anCase+noise+title+'.pdf')




plt.show()
print('Done Analysis2D')
